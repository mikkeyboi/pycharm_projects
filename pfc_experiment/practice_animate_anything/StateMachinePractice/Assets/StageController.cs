﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageController : MonoBehaviour {

    public Button BeginStage;
    public Button Next;
    public Button Reset;
    public TextMesh OnScreenText;
    public Animator animator;

    // Use this for initialization
    void Start() {
        BeginStage.GetComponent<Button>().onClick.AddListener(TaskOnClickBeginStage);
        Next.GetComponent<Button>().onClick.AddListener(TaskOnClickNext);
        Reset.GetComponent<Button>().onClick.AddListener(TaskOnClickReset);
        OnScreenText.text = "Click Begin Stage to Begin!";
    }

    void TaskOnClickBeginStage()
    {
        animator.SetTrigger("BeginClicked");
    }

    void TaskOnClickNext()
    {
        animator.SetTrigger("NextClicked");
    }


    // Any State Reset Button
    void TaskOnClickReset()
    {
        animator.SetTrigger("AnyStateReset");
    }

    // Update is called once per frame
    void Update()
    {
        // AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(layer_index);
        // if (info.IsName("Cue3"))
    }
}
        
      