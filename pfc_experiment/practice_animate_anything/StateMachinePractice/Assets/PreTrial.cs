﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreTrial : StateMachineBehaviour
{

    private float preTrialDuration;
    private string animTimerName = "PreTrialRemaining";

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        preTrialDuration = animator.GetFloat("PreTrialDuration");
        updateAnimatorTimer(animator, 0.0F);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorTimer(animator, animationState.normalizedTime);
        TextMesh OnScreenText = GameObject.Find("OnScreenText").GetComponent<TextMesh>();
        OnScreenText.text = "Initializing ... Wait " + animator.GetFloat(animTimerName).ToString("F1") + " more seconds.";
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        updateAnimatorTimer(animator, 0.0F);
    }

    private void updateAnimatorTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, preTrialDuration - newTime);
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
