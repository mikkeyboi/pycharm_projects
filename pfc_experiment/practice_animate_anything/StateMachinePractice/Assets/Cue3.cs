﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cue3 : StateMachineBehaviour {
    // TODO: Own the colour buttons.

    // public Button RedButton;
    public Canvas ColorSelect;
    public GameObject FloatingCube;
    private GameObject myFloatingCube;
    private string cue3RequirementName = "ColorIndex";

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        // Cue Instructions
        TextMesh OnScreenText = GameObject.Find("OnScreenText").GetComponent<TextMesh>();
        OnScreenText.text = "Stage 3: Walk away from tree and click red!";

        // Create floating cube - (Cannot modify the instantiated prefab hence "myFloatingCube")
        GameObject parent = GameObject.Find("ThirdPersonController");
        myFloatingCube = GameObject.Instantiate(FloatingCube, parent.transform);

        // Create canvas for the colors
        GameObject.Instantiate(ColorSelect, new Vector3(500, -75), Quaternion.identity);

        // Create Listeners for each button
        GameObject.Find("CanvasRed").GetComponent<Button>().onClick.AddListener( () =>
        {
            updateColour(animator, 1, Color.red);
        });
        GameObject.Find("CanvasGreen").GetComponent<Button>().onClick.AddListener(() =>
        {
            updateColour(animator, 2, Color.green);
        });
        GameObject.Find("CanvasBlue").GetComponent<Button>().onClick.AddListener(() =>
        {
            updateColour(animator, 3, Color.blue);
        });
    }


    // Change color parameter in State Machine based on Boolean operator
    void updateColour(Animator animator, int newIndex, Color newColor)
    {
        // Redefine floating cube
        int lastIndex = animator.GetInteger(cue3RequirementName); // Error: Object reference not set to an instance of an object
        if (lastIndex == newIndex)
        {
            animator.SetInteger(cue3RequirementName, 0);
            myFloatingCube.GetComponent<Renderer>().material.color = Color.white;
        }
        else
        {
            animator.SetInteger(cue3RequirementName, newIndex);
            myFloatingCube.GetComponent<Renderer>().material.color = newColor;
        }
    }


	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
