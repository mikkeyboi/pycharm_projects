﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeginStage : StateMachineBehaviour {

    public float stateDuration = 3.0F;
    private string animTimerName = "DelayToStartCue";

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        TextMesh OnScreenText = GameObject.Find("OnScreenText").GetComponent<TextMesh>();
        OnScreenText.text = "Initializing ... Wait " + animator.GetFloat("DelayToStartCue").ToString("F1") + " seconds.";
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        updateAnimatorCueTimer(animator, 0);
        Debug.Log("BeginStage:OnStateExit");
    }

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, stateDuration - newTime);
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
