﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cue2 : StateMachineBehaviour {

    private float cue2Duration;
    private float timeInState = 0.0F;
    private string animTimerName = "Cue2Remaining";

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        cue2Duration = animator.GetFloat("Cue2Duration");
        timeInState = 0.0F;
        updateAnimatorTimer(animator, timeInState);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // TODO: get reference to tree behaviour
        TreeProximity proximityBehaviour = GameObject.Find("Tree").GetComponent<TreeProximity>();
        if (proximityBehaviour.NearTree)
        {
            timeInState += Time.deltaTime;
        }
        updateAnimatorTimer(animator, timeInState);
        TextMesh OnScreenText = GameObject.Find("OnScreenText").GetComponent<TextMesh>();
        OnScreenText.text = "Stage 2: Stay near tree for " + animator.GetFloat(animTimerName).ToString("F1") + " more seconds.";
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        timeInState = 0.0F;
        updateAnimatorTimer(animator, timeInState);
    }

    private void updateAnimatorTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, cue2Duration - newTime);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
