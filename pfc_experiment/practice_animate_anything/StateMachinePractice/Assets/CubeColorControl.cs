﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Code pasted into Cue3.cs state behavior

public class CubeColorControl : MonoBehaviour {

    // public Button RedButton;
    private Button RedButton;
    public Button BlueButton;
    public Button GreenButton;
    public GameObject Cube;
    private Animator animator;
    private string cue3RequirementName = "ColorIndex";

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

	// Use this for initialization

	void Start () {
        // Create Listeners for each button
        GameObject.Find("To Red").GetComponent<Button>().onClick.AddListener(TaskOnClickRed);
        RedButton.GetComponent<Button>().onClick.AddListener(TaskOnClickRed);
        BlueButton.GetComponent<Button>().onClick.AddListener(TaskOnClickBlue);
        GreenButton.GetComponent<Button>().onClick.AddListener(TaskOnClickGreen);

    }

    void Update()
    {
       
    }
    
    // Change color parameter in State Machine based on Boolean operator
    void updateColour(int newIndex, Color newColor)
    {
        int lastIndex = animator.GetInteger(cue3RequirementName);
        if (lastIndex == newIndex)
        {
            animator.SetInteger(cue3RequirementName, 0);
            Cube.GetComponent<Renderer>().material.color = Color.white;
        }
        else
        {
            animator.SetInteger(cue3RequirementName, newIndex);
            Cube.GetComponent<Renderer>().material.color = newColor;
        }
    }

    void TaskOnClickRed()
    {
        updateColour(1, Color.red);
    }

    void TaskOnClickBlue()
    {
        updateColour(3, Color.blue);
    }

    void TaskOnClickGreen()
    {
        updateColour(2, Color.green);
    }

}
