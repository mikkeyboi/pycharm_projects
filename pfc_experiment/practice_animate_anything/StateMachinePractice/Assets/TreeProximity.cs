﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeProximity : MonoBehaviour {

    public bool NearTree = false;

	// Use this for initialization

    void Start()
    {
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            NearTree = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            NearTree = false;
        }
    }

	// Update is called once per frame
	void Update ()
    {
	}
}
