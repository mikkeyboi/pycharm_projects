﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Cue1 : StateMachineBehaviour {

    private int stateRequirement;
    private string stateCounterName = "Cue1Remaining";
    public Canvas ClickSelect;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        stateRequirement = animator.GetInteger("Cue1Requirement");
        animator.SetInteger(stateCounterName, stateRequirement);
        
        // TODO: How do I own my own Button - Apparently you cannot access animator parameters in StateMachineBehavior
        GameObject.Instantiate(ClickSelect, new Vector3(0.0f, 5.0f), Quaternion.identity);
        GameObject.Find("ClickMeter").GetComponent<Button>().onClick.AddListener(() =>
        {
            int clickCountDown = animator.GetInteger("Cue1Remaining");
            clickCountDown--;
            animator.SetInteger("Cue1Remaining", clickCountDown);
        });

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        TextMesh OnScreenText = GameObject.Find("OnScreenText").GetComponent<TextMesh>();
        OnScreenText.text = "Stage 1: Click " + animator.GetInteger(stateCounterName) + " more times!";
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetInteger(stateCounterName, animator.GetInteger("Cue1Requirement"));
        Destroy(GameObject.Find("ClickMeter"));
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
