﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cue1 : StateMachineBehaviour {

    private Transform ducky;
    private Transform kitty;
    private Text OnScreenText;
    private int DuckTracker;
    private GameObject[] Fixation;
    private GameObject CentralFixation;
    private string fixationTimerName = "StateTimer";
    public GameObject DuckPrefab;
    public GameObject CatPrefab;
    public float stateDuration = 4.0f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {


        // Make RNG
        var leftright = Random.Range(1, 255);
        var chooser = Random.Range(1, 255);


        // RNG for Left or Right Position for Ducky and Kitty
        if (leftright %2 == 0)
        {
            // Left Duck Right Cat
            GameObject leftparent = GameObject.Find("LeftPosition");
            GameObject.Instantiate(DuckPrefab, leftparent.transform);
            GameObject rightparent = GameObject.Find("RightPosition");
            GameObject.Instantiate(CatPrefab, rightparent.transform);
            // Assign Position Tracker Logic
            DuckTracker--;
            animator.SetFloat("DuckTracker", DuckTracker);
        }
        else
        {
            // Right Duck Left Cat
            GameObject leftparent = GameObject.Find("LeftPosition");
            GameObject.Instantiate(CatPrefab, leftparent.transform);
            GameObject rightparent = GameObject.Find("RightPosition");
            GameObject.Instantiate(DuckPrefab, rightparent.transform);
            // Assign Position Tracker Logic
            DuckTracker++;
            animator.SetFloat("DuckTracker", DuckTracker);
        }

        // Find In-Game Objects
        ducky = GameObject.Find("ducky(Clone)").GetComponent<Transform>();
        kitty = GameObject.Find("kitty(Clone)").GetComponent<Transform>();

        // Boolean for RNG (Ducky vs Kitty)
        if (chooser % 2 == 0)
        {
            ducky.localScale += new Vector3(0.20f, 0.20f, 0.20f);
            // RNG for No-Go
            if (animator.GetInteger("TaskIndex") > 0)
            {
                // duckywins or kittywins are operators that trigger advance next stage
                // Also to know if the user made an incorrect choice
                animator.SetBool("duckywins", true);
            }
            else
                if (animator.GetInteger("TaskIndex") == 0)
                {
                    // Trigger No-go Fixation
                    animator.SetTrigger("Begin No-go Fixation");
                }
        }
        if (chooser % 2 == 1)
        {
            kitty.localScale += new Vector3(0.20f, 0.20f, 0.20f);
            if (animator.GetInteger("TaskIndex") > 0)
            {
                animator.SetBool("kittywins", true);
            }
            else
                if (animator.GetInteger("TaskIndex") == 0)
                {
                    // Trigger No-go Fixation
                    animator.SetTrigger("Begin No-go Fixation");
                }
            
        }

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        int incorrectMeter;
        OnScreenText = Text.FindObjectOfType<Text>();
        OnScreenText.text = "Use Arrow Keys To Select Bigger Animal!";

        // If Correct Response, Advance
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            if (animator.GetBool("duckywins"))
            {
                if (animator.GetFloat("DuckTracker") > 0)
                {
                    incorrectMeter = animator.GetInteger("IncorrectMeter");
                    incorrectMeter++;
                    animator.SetInteger("IncorrectMeter", incorrectMeter);
                    // Reset Stage
                    animator.SetTrigger("ResetTrigger");
                }
                else
                {
                    animator.SetTrigger("AdvanceNextStage");
                }
            }
            if (animator.GetBool("kittywins"))
            {
                if (animator.GetFloat("DuckTracker") > 0)
                {
                    animator.SetTrigger("AdvanceNextStage");
                }
                else
                {
                    incorrectMeter = animator.GetInteger("IncorrectMeter");
                    incorrectMeter++;
                    animator.SetInteger("IncorrectMeter", incorrectMeter);
                    // Reset Stage
                    animator.SetTrigger("ResetTrigger");
                }
                
            } 

        }

        // Allow user to make saccade by destroying fixation
        if (animator.GetFloat("StateTimer") < 3)
        {
            Fixation = GameObject.FindGameObjectsWithTag("Fixation");
            for (var i = 0; i < Fixation.Length; i++)
            {
                Destroy(Fixation[i]);
            }
        }

        // If Correct Response, Advance
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (animator.GetBool("duckywins"))
            {
                if (animator.GetFloat("DuckTracker") > 0)
                {
                    animator.SetTrigger("AdvanceNextStage");
                }
                else
                {
                    incorrectMeter = animator.GetInteger("IncorrectMeter");
                    incorrectMeter++;
                    animator.SetInteger("IncorrectMeter", incorrectMeter);
                    // Reset Stage
                    animator.SetTrigger("ResetTrigger");
                }
            }
            if (animator.GetBool("kittywins"))
            {
                if (animator.GetFloat("DuckTracker") > 0)
                {
                    incorrectMeter = animator.GetInteger("IncorrectMeter");
                    incorrectMeter++;
                    animator.SetInteger("IncorrectMeter", incorrectMeter);
                    // Reset Stage
                    animator.SetTrigger("ResetTrigger");
                }
                else
                {
                    animator.SetTrigger("AdvanceNextStage");
                }

            }
        }

        // Terminate trial if user takes too long
        if (animator.GetFloat("StateTimer") < 1)
        {
            incorrectMeter = animator.GetInteger("IncorrectMeter");
            incorrectMeter++;
            animator.SetInteger("IncorrectMeter", incorrectMeter);
            // Reset Stage
            animator.SetTrigger("ResetTrigger");
        }
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        OnScreenText = Text.FindObjectOfType<Text>();
        OnScreenText.text = "Trial ending in " + animator.GetFloat("StateTimer").ToString("F1") + " seconds.";

    }

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(fixationTimerName, stateDuration - newTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (animator.GetFloat("DuckTracker") > 0)
        {
            DuckTracker--;
            animator.SetFloat("DuckTracker", DuckTracker);
        }
        else
        {
            DuckTracker++;
            animator.SetFloat("DuckTracker", DuckTracker);
        }

        if (animator.GetBool("duckywins"))
        {
            animator.SetBool("duckywins", false);

        }
        if (animator.GetBool("kittywins"))
        {
            animator.SetBool("kittywins", false);

        }


        updateAnimatorCueTimer(animator, 0);
    }

}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
