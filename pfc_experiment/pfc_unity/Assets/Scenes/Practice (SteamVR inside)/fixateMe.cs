﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fixateMe : MonoBehaviour {

    public Vector3 centralFixation; // Attach this script to Central Fixation, if not, then find

    // Use this for initialization
    void Start() {
        centralFixation = gameObject.transform.position; // Find (this) object to place collider sphere radius
    }

    // Update is called once per frame
    void Update() {
        fixateMeCollider(centralFixation, 0.5f); // Check if there's anything colliding with this object
    }

    void fixateMeCollider(Vector3 center, float radius)
    {
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
            hitColliders[i].SendMessage("collidingFixation"); // Call this method for every collide-able gameobject within the radius
            i++;
        }
    }

}
