﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;


public class ExperimentController : MonoBehaviour {

    public Dropdown trialSelector;
    public Animator animator;
    public GameObject leftPrefab; // Lasers
    public GameObject rightPrefab; // Lasers
    public GameObject colliderCubePrefab;
    public static ExperimentController instance = null; // For scripts to access methods here
    public float animalHitBox = 0.1f;
    // Experiment Information for access
    public Vector3 leftPosition = new Vector3(-4.3f, 0.8f, 0.38f); // TODO: Map with visual angle
    public Vector3 rightPosition = new Vector3(-4.3f, 0.8f, -0.38f);
    public Vector3 cuePosition = new Vector3(-4.3f, 0.8f, 0.0f);
    public GameObject cubeIsTargeting;
    // public float visualAngle = 0.0f; // TODO: this modifies left right positions
    public AudioClip correctSound;
    public AudioClip incorrectSound;

    private GameObject colliderCube;
    private GameObject ducky;
    private GameObject kitty;
    private Button beginButton;
    private GameObject centralFixation; // Central Fixation point
    private float colliderRadius = 0.13f; // Radius of overlap sphere fixation collider (Visualize by gizmos)
    private Camera sceneCamera;
    private LineRenderer leftLaser;
    private LineRenderer rightLaser;


    void Start () {
        // Stage Index Controller
        trialSelector.onValueChanged.AddListener(delegate {
            myDropdownValueChangedHandler(trialSelector);
        });

        // Setup Button
        beginButton = GameObject.Find("Begin Button").GetComponent<Button>();
        beginButton.onClick.AddListener(OnClickBegin);

        // Where the fixation point is going to be
        centralFixation = GameObject.Find("CentralFixation");
        // Setup Central Fixation
        centralFixation.AddComponent<ColliderScript>();
        SphereCollider centralSphere = centralFixation.AddComponent<SphereCollider>();
        centralSphere.radius = animalHitBox;

        // Find camera
        sceneCamera = Camera.main;

        // Temp - Instantiate Lasers
        Instantiate(leftPrefab);
        Instantiate(rightPrefab);
        leftLaser = GameObject.Find("LeftLaser(Clone)").GetComponent<LineRenderer>();
        rightLaser = GameObject.Find("RightLaser(Clone)").GetComponent<LineRenderer>();

        // Spawn collider cube
        Instantiate(colliderCubePrefab);
        colliderCube = GameObject.Find("Cube(Clone)");
        colliderCube.AddComponent<ColliderScript>();
        colliderCube.AddComponent<SphereCollider>().isTrigger = true;
        colliderCube.AddComponent<Rigidbody>();

        // Setup Left/Right Components
        ducky = GameObject.Find("Duck");
        kitty = GameObject.Find("Cat");
        ducky.AddComponent<SphereCollider>().radius = animalHitBox;
        ducky.GetComponent<SphereCollider>().center = new Vector3(0.0f, 0.1f);
        kitty.AddComponent<SphereCollider>().radius = animalHitBox;
        kitty.GetComponent<SphereCollider>().center = new Vector3(0.0f, 0.1f);

        // Synchronize animalhitbox value with input field in GUI
        InputField guiInputField = GameObject.Find("animalHitBoxValue").GetComponent<InputField>();
        guiInputField.onEndEdit.AddListener(UpdateAnimalHitBox);

    }

    private void myDropdownValueChangedHandler(Dropdown target)
    {
        Debug.Log("selected: " + target.value);
        animator.SetInteger("StageIndex", target.value);
    }

    public void SetDropdownIndex(int index)
    {
        trialSelector.value = index;

    }

    void Destroy()
    {
        trialSelector.onValueChanged.RemoveAllListeners();
    }

    // Update animal hit box size after input
    void UpdateAnimalHitBox(string newHitBoxValue)
    {
        animalHitBox = float.Parse(newHitBoxValue); // Gizmos immediately updates
        ducky.GetComponent<SphereCollider>().radius = animalHitBox;
        kitty.GetComponent<SphereCollider>().radius = animalHitBox;
        centralFixation.GetComponent<SphereCollider>().radius = animalHitBox;
        Debug.Log("The new hitbox value is now " + newHitBoxValue);
    }

    public void Awake() // give access to other scripts if other scripts need to use methods in here
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(GameObject.Find("CentralFixation").transform.position, animalHitBox); // Needs reference GameObject.Find since this draws even when game is not playing
        Gizmos.DrawWireSphere(GameObject.Find("Duck").transform.position + new Vector3(0.0f, 0.1f), animalHitBox);
        Gizmos.DrawWireSphere(GameObject.Find("Cat").transform.position + new Vector3(0.0f, 0.1f), animalHitBox); 
    }

    void Update()
    {
        // Temp: Laser Code from PupilController - if I can't access Ray information from here, then I'll put all the laser code here instead
        // Re-align openVR camera coordinates to world camera coordinates for accurate transform-data for left/right eye positions
        Matrix4x4 m = Camera.main.cameraToWorldMatrix;
        Vector3 left = Quaternion.Inverse(UnityEngine.XR.InputTracking.GetLocalRotation(UnityEngine.XR.XRNode.LeftEye)) * UnityEngine.XR.InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.LeftEye);
        Vector3 right = Quaternion.Inverse(UnityEngine.XR.InputTracking.GetLocalRotation(UnityEngine.XR.XRNode.RightEye)) * UnityEngine.XR.InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.RightEye);
        Vector3 offset = (left - right) * 0.5f;
        Vector3 leftWorld = m.MultiplyPoint(-offset);
        Vector3 rightWorld = m.MultiplyPoint(offset);
        // Make my laser-eyes
        Vector3 viewportPoint = new Vector3(0.5f, 0.5f, 10f);

        Ray ray = sceneCamera.ViewportPointToRay(viewportPoint);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && !hit.collider.isTrigger)
        {
            leftLaser.SetPosition(0, leftWorld);
            leftLaser.SetPosition(1, hit.point);
            rightLaser.SetPosition(0, rightWorld);
            rightLaser.SetPosition(1, hit.point);
        }

        // Make Ideal Line to see Angular Difference
        Vector3 centralDot = GameObject.Find("CentralFixation").transform.position;
        Debug.DrawLine(sceneCamera.transform.position, centralDot);
        // Move cube towards hitpoint destinations
        Transform colliderCubePosition = GameObject.Find("Cube(Clone)").GetComponent<Transform>();
        colliderCubePosition.position = leftLaser.GetPosition(1);
        // Find angle between Gaze and Central Dot
        // float angularDifference;
        // angularDifference = Vector3.Angle(centralDot, hit.point);
        // Debug.Log(angularDifference);

    }


    public void colliderCubeSelect(GameObject animal) // Retrieves information from colliderscript
    {
        Debug.Log("You are currently hitting " + animal.name);
        cubeIsTargeting = animal; // For state machine to ask this controller what is being hit
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Response Phase"))
        {
            Debug.Log("In Response Phase");
            animator.SetBool("animalSelected", true); // Event to continue with the state machine progression
        }

    }

    void OnClickBegin()
    {
        animator.SetTrigger("Begin Trial");
    }

    // Sounds
    public void CorrectResponse()
    {
        GetComponent<AudioSource>().PlayOneShot(correctSound, 0.7f);
    }

    public void IncorrectResponse()
    {
        GetComponent<AudioSource>().PlayOneShot(incorrectSound, 0.7f);
    }


}


// Abstract Class: Target - contain functions that will change various properties.

public abstract class Target : MonoBehaviour
{
    public int kittyColor;
    public int duckColor;


    private Transform kittyteleport;
    private Transform duckteleport;
    private kittyscript invisCat;
    private duckscript invisDuck;
    private Transform ducky;
    private Transform kitty;

    public abstract void SetAnimalProperty(); // Use to apply misc. changes

    public virtual void MakeInvisible(bool setBool = false)
    {

        // Find Objects
        // TODO: Perhaps label both animals as "Animals" and change transform property using for loop (foreach)
        duckteleport = GameObject.Find("Duck").GetComponent<Transform>();
        kittyteleport = GameObject.Find("Cat").GetComponent<Transform>();

        if (setBool == true)
        {
            // Make objects invisible (teleport away from room)
            duckteleport.Translate(new Vector3(10.0f, 10.0f, 10.0f));
            kittyteleport.Translate(new Vector3(10.0f, 10.0f, 10.0f));
        }
        else
        {
            // Make objects visible (teleport back to default positions)
            duckteleport.Translate(new Vector3(-10.0f, -10.0f, -10.0f));
            kittyteleport.Translate(new Vector3(-10.0f, -10.0f, -10.0f));
        }

    }

    // Parent setter script
    public void SetParent(GameObject newParent, GameObject animal)
    {
        // Assign to left or right parent positions (pre-stored rotation)
        animal.transform.parent = newParent.transform;
        // If an animal switches from left to right, it'll store its original position regardless of new parent. So we set the animal's position and rotation to 0 respective to parent
        animal.GetComponent<Transform>().localRotation = Quaternion.identity;
        animal.GetComponent<Transform>().localPosition = new Vector3(0.0f, 0.0f, 0.0f);

    }

    // Position setter script
    public void SetPosition(GameObject animal, bool isLeft)
    {
        ExperimentController expControl = GameObject.Find("ExperimentController").GetComponent<ExperimentController>();
        // Find L / R positions
        Vector3 leftPosition = expControl.leftPosition;
        Vector3 rightPosition = expControl.rightPosition;
        // Rotation setter
        Quaternion leftRotation = Quaternion.Euler(new Vector3(0.0f, -122.5f, 0.0f));
        Quaternion rightRotation = Quaternion.Euler(new Vector3(0.0f, -45.0f, 0.0f));
        if (isLeft == true)
        {
            animal.transform.position = leftPosition;
            animal.transform.rotation = Quaternion.identity;
            animal.transform.rotation = leftRotation;
        }
        else
        {
            animal.transform.position = rightPosition;
            animal.transform.rotation = Quaternion.identity;
            animal.transform.rotation = rightRotation;
        }

    }

    // Color Changer Delegate
    public delegate void ChangeColorHandler();

    // Color Changer Event
    public static event ChangeColorHandler blackDuck;
    public static event ChangeColorHandler yellowDuck;
    public static event ChangeColorHandler colorReset;

    public void OnTaskIndex2(int newColor)
    {
        if (newColor == 0)
        {
            blackDuck();
        }
        if (newColor == 1)
        {
            yellowDuck();
        }
        if (newColor == 2)
        {
            colorReset();
        }
    }


}

// Class Animals: Inherits from abstract class. All animals will inherit from this class

public class Animals : Target
{
    public static Animals instance = null; // Singleton creation / Allow access from other scripts

    public void Awake ()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            // Destroy(gameObject.GetComponent<Animals>());
        }
    }

    public override void SetAnimalProperty()
    {
        // Reserve for misc. modifications
        Debug.Log("Initiating Abstract Class");
    }
}


