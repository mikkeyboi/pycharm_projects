﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

public class PupilController : MonoBehaviour {

    private Vector3 standardViewportPoint = new Vector3(0.5f, 0.5f, 10);
    private Camera sceneCamera;
    private Vector2 gazePointLeft;
    private Vector2 gazePointRight;
    private Vector2 gazePointCenter;
    private Text OnScreenText;
    private bool laserEyes = false;
    private LineRenderer leftLaser;
    private LineRenderer rightLaser;
    // Gaze Data to make invisible to patient
    private GameObject leftEye2D;
    private GameObject rightEye2D;
    private GameObject gaze2D;
    
    public float interpupillaryDistance = 0.06f;
    public GameObject leftPrefab; // Left Laser Prefab
    public GameObject rightPrefab;

    // GUI Button(s)
    private Button calibrationButton;
    private Button startPupilLabsEyes;
    private GameObject[] bonusUIDestroy; // Get rid of Start pupil button

    // Pupil Labs
    private PupilGazeTracker pupilTracker;

    void Start () {

        // Locate objects
        calibrationButton = GameObject.Find("Calibrate Button").GetComponent<Button>();
        startPupilLabsEyes = GameObject.Find("StartPupilLabs").GetComponent<Button>();
        sceneCamera = GameObject.Find("Main Camera").GetComponentInChildren<Camera>();
        bonusUIDestroy = GameObject.FindGameObjectsWithTag("BonusUI");

        // Button Listeners
        calibrationButton.onClick.AddListener(OnClickCalibrate); // Calibration button listener
        startPupilLabsEyes.onClick.AddListener(OnClickStartPupilEyes);


        // Temp laser code (Default: Lasers instantiates after calibration ends)
        // Instantiate(leftPrefab);
        // Instantiate(rightPrefab);
        // leftLaser = GameObject.Find("LeftLaser(Clone)").GetComponent<LineRenderer>();
        // rightLaser = GameObject.Find("RightLaser(Clone)").GetComponent<LineRenderer>();

    }

    void OnClickStartPupilEyes()
    {
        // Destroy Button
        for (var i = 0; i < bonusUIDestroy.Length; i++)
        {
            Destroy(bonusUIDestroy[i]);
        }
        
        // Initiating Pupil Eyes
        pupilTracker = PupilGazeTracker.Instance;

        // Indicate on my GUI the status of these three states (Connected, Calibrate Start and End)
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        PupilTools.OnConnected += OnConnect;
        PupilTools.OnCalibrationStarted += OnCalibrateStart;
        PupilTools.OnCalibrationEnded += OnCalibrateEnd;
    }

    void OnClickCalibrate()
    {
        // Calibrate start/stop multifunction button
        if (PupilSettings.Instance.dataProcess.state == PupilSettings.EStatus.Calibration)
        {
            PupilTools.StopCalibration();
        }
        else
        {
            PupilTools.StartCalibration();
        }
    }
	
    void OnConnect()
    {
        // Update my GUI message that Pupil Labs is connected
        OnScreenText.text = "Pupil is Connected";

    }

    void OnCalibrateStart()
    {
        // Indicate Calibration has started
        OnScreenText.text = "Calibration has started";
    }

    void OnCalibrateEnd()
    {
        // Indicate Calibration has ended
        OnScreenText.text = "Calibration has ended";

        // Make left and right lasers
        Instantiate(leftPrefab);
        Instantiate(rightPrefab);
        leftLaser = GameObject.Find("LeftLaser(Clone)").GetComponent<LineRenderer>();
        rightLaser = GameObject.Find("RightLaser(Clone)").GetComponent<LineRenderer>();

        // Load GazeData
        GazeData();

    }

    void GazeData()
    {
        // Spawn three bullseye targets
        if (PupilSettings.Instance.connection.isConnected && PupilSettings.Instance.dataProcess.state == PupilSettings.EStatus.ProcessingGaze)
        {
            pupilTracker.StartVisualizingGaze();
        }

        // Mark Bullseye objects for Experimenter view only
        leftEye2D = GameObject.Find("LeftEye_2D");
        rightEye2D = GameObject.Find("RightEye_2D");
        gaze2D = GameObject.Find("Gaze_2D");
        leftEye2D.layer = LayerMask.NameToLayer("Experimenter");
        rightEye2D.layer = LayerMask.NameToLayer("Experimenter");
        gaze2D.layer = LayerMask.NameToLayer("Experimenter");


        // Spawn laser
        laserEyes = true;

        // Below: For anything that I want started shortly after calibration ends
    }

	// Update is called once per frame
	void Update () {
        // Re-align openVR camera coordinates to world camera coordinates for accurate transform-data for left/right eye positions
        Matrix4x4 m = Camera.main.cameraToWorldMatrix;
        Vector3 left = Quaternion.Inverse(UnityEngine.XR.InputTracking.GetLocalRotation(UnityEngine.XR.XRNode.LeftEye)) * UnityEngine.XR.InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.LeftEye);
        Vector3 right = Quaternion.Inverse(UnityEngine.XR.InputTracking.GetLocalRotation(UnityEngine.XR.XRNode.RightEye)) * UnityEngine.XR.InputTracking.GetLocalPosition(UnityEngine.XR.XRNode.RightEye);
        Vector3 offset = (left - right) * 0.5f;
        Vector3 leftWorld = m.MultiplyPoint(-offset);
        Vector3 rightWorld = m.MultiplyPoint(offset);


        // Make my laser-eyes
        if (laserEyes == true)
        {
            Vector3 viewportPoint = standardViewportPoint;
            if (PupilSettings.Instance.connection.isConnected && PupilSettings.Instance.dataProcess.state == PupilSettings.EStatus.ProcessingGaze)
            {
                gazePointLeft = PupilData._2D.GetEyePosition(sceneCamera, PupilData.GazeSource.LeftEye);
                gazePointRight = PupilData._2D.GetEyePosition(sceneCamera, PupilData.GazeSource.RightEye);
                gazePointCenter = PupilData._2D.GetEyePosition(sceneCamera, PupilData.GazeSource.BothEyes);
                viewportPoint = new Vector3(gazePointCenter.x, gazePointCenter.y, 0.8f);
            }

            Ray ray = sceneCamera.ViewportPointToRay(viewportPoint);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                leftLaser.SetPosition(0, leftWorld); // original: sceneCamera.transform.position - sceneCamera.transform.up
                leftLaser.SetPosition(1, hit.point);
                rightLaser.SetPosition(0, rightWorld);
                rightLaser.SetPosition(1, hit.point);
            }

            // Move cube towards hitpoint destinations
            Transform colliderCubePosition = GameObject.Find("Cube(Clone)").GetComponent<Transform>();
            colliderCubePosition.position = leftLaser.GetPosition(1);
        


        }




    }
}
