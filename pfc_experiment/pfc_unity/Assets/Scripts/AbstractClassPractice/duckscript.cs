﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class duckscript : Animals {

    Color[] colors = new Color[3];

    void Start()
    {
        // Define color array
        colors[0] = new Color(0.1f, 0.1f, 0.16f); // black
        colors[1] = new Color(1.0f, 0.98f, 0.50f); // yellow
        colors[2] = new Color(1.0f, 0.32f, 0.32f); // red
    }
    
    void OnEnable()
    {
        blackDuck += ChangeBlack;
        yellowDuck += ChangeYellow;
        colorReset += ColorReset;
    }

    void ChangeBlack()
    {
        ChangeColor(0);
    }

    void ChangeYellow()
    {
        ChangeColor(1);
    }

    void ChangeColor(int duckColor) // TODO: Make Delegate to trigger when TaskIndex is 2
    {
        // Find rendering components to change color
        Renderer rendbody = GetComponentInChildren<SkinnedMeshRenderer>(); // Body
        rendbody.material.shader = Shader.Find("Specular");
        rendbody.material.SetColor("_Color", colors[duckColor]);
        Renderer rendhead = GetComponentInChildren<Renderer>(); // Head
        rendhead.material.shader = Shader.Find("Specular");
        rendhead.material.SetColor("_Color", colors[duckColor]);
        Renderer rendwingL = GameObject.Find("wing_L").GetComponent<MeshRenderer>(); // Left wing
        rendwingL.material.shader = Shader.Find("Specular");
        rendwingL.material.SetColor("_Color", colors[duckColor]);
        Renderer rendwingR = GameObject.Find("wing_R").GetComponent<MeshRenderer>(); // Right wing
        rendwingR.material.shader = Shader.Find("Specular");
        rendwingR.material.SetColor("_Color", colors[duckColor]);
    }

    void ColorReset ()
    {
        Renderer rendbody = GetComponentInChildren<SkinnedMeshRenderer>(); // Body
        rendbody.material.shader = Shader.Find("Unlit/Texture");
        Renderer rendhead = GetComponentInChildren<Renderer>(); // Head
        rendhead.material.shader = Shader.Find("Unlit/Texture"); 
        Renderer rendwingL = GameObject.Find("wing_L").GetComponent<MeshRenderer>(); // Left wing
        rendwingL.material.shader = Shader.Find("Unlit/Texture");
        Renderer rendwingR = GameObject.Find("wing_R").GetComponent<MeshRenderer>(); // Right wing
        rendwingR.material.shader = Shader.Find("Unlit/Texture");
    }

    void OnDisable()
    {
        blackDuck -= ChangeBlack;
        yellowDuck -= ChangeYellow;
        colorReset -= ColorReset;
    }
}
