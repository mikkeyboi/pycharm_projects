﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayRules : StateMachineBehaviour {

    public Toggle useControllerToggle;
    private Transform guiHierarchy;
    private Text OnScreenText;
    private GameObject[] bonusUIDestroy;
    private float controllerRadius = 0.035f;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Look at the larger target, \n but maintain central fixation when black sphere shows up!";
        // Make Use Controller Toggle
        guiHierarchy = GameObject.Find("GUI Menu").GetComponent<Transform>();
        Instantiate(useControllerToggle, guiHierarchy);
        // Make Collider on right controller
        GameObject rightController = GameObject.Find("Controller (right)");
        if (rightController) // Won't cause errors if controller is off
        {
            Toggle isToggleOn = GameObject.Find("Use Controller Toggle(Clone)").GetComponent<Toggle>();
            // Debug.Log("Adding controller Sphere Collider");
            rightController.AddComponent<SphereCollider>().radius = controllerRadius;
            rightController.AddComponent<ColliderScript>(); // Vibration 
            // Make Toggle listener in case we don't need to use the controller
            isToggleOn.onValueChanged.AddListener(useControllerToggler);
        }



    }

    void useControllerToggler (bool isDestroy)
    {
        // Destroy controller-collider capabilities
        GameObject rightController = GameObject.Find("Controller (right)");
        if (rightController) // Won't cause errors if controller is off
        {
            if (isDestroy == false)
            {
                Destroy(rightController.GetComponent<SphereCollider>());
                Destroy(rightController.GetComponent<ColliderScript>());
            }
            // Restore controller-collider capabilities
            else
            {
                rightController.AddComponent<SphereCollider>().radius = controllerRadius;
                rightController.AddComponent<ColliderScript>();
            }
        }

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	// override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	// }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Debug Messages";
        // We're done with the controller decision
        bonusUIDestroy = GameObject.FindGameObjectsWithTag("BonusUI");
        for (var i = 0; i < bonusUIDestroy.Length; i++)
        {
            Destroy(bonusUIDestroy[i]);
        }

    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
