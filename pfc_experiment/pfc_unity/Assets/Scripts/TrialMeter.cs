﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrialMeter : StateMachineBehaviour {

    public int addTrial;
    private int TaskIndex;
    private int animColor;
    private GameObject[] Fixation;
    private GameObject[] DestroyBegin;
    private float DuckTracker;
    private DisplayRules abstractclass;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        // Destroy all unnecessary objects (Paintings, furniture, etc.)
        Fixation = GameObject.FindGameObjectsWithTag("Fixation");
        for (var i = 0; i < Fixation.Length; i++)
        {
            Destroy(Fixation[i]);
        }

        DestroyBegin = GameObject.FindGameObjectsWithTag("DestroyBegin");
        for (var i = 0; i < DestroyBegin.Length; i++)
        {
            Destroy(DestroyBegin[i]);
        }

        // Reset parameter trackers
        if (animator.GetBool("duckyWin"))
        {
            animator.SetBool("duckyWin", false);

        }
        if (animator.GetBool("kittyWin"))
        {
            animator.SetBool("kittyWin", false);

        }
        if (animator.GetBool("animalSelected"))
        {
            animator.SetBool("animalSelected", false);
        }
        if (animator.GetBool("NoGoWin"))
        {
            animator.SetBool("NoGoWin", false);
        }

        // Make Duck and Cat Disappear
        Animals.instance.MakeInvisible(true);

        // Reset DuckTracker
        DuckTracker = animator.GetFloat("DuckTracker");
        if (animator.GetFloat("DuckTracker") > 0)
        {
            DuckTracker--;
            animator.SetFloat("DuckTracker", DuckTracker);
        }
        if (animator.GetFloat("DuckTracker") < 0)
        {
            DuckTracker++;
            animator.SetFloat("DuckTracker", DuckTracker);
        }

        // +1 trial counter
        addTrial = animator.GetInteger("TrialMeter");
        addTrial++;
        animator.SetInteger("TrialMeter", addTrial);
        animator.SetTrigger("AdvanceNextStage");

        // Reset Color of Animals
        Animals.instance.OnTaskIndex2(2);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Initiate Task Selector
        // RNG Task Type:
        // 1 = Species Task
        // 2 = Color Task
        // 3 = Big or Small

        TaskIndex = Random.Range(1, 3); // 
        animator.SetInteger("TaskIndex", TaskIndex);

        // Pre-determine color of animals
        animColor = Random.Range(0, 2);

        // Pre-determine animal position
        var leftrightposition = Random.Range(1, 255);

        // Write to GameObject: ExperimentProps - keeps track of color and leftright position
        GameObject storeColors = GameObject.Find("ExperimentProps");
        colorDetermine colorDetermine = storeColors.GetComponent<colorDetermine>();
        colorDetermine.leftrightposition = leftrightposition;

        // What is the task's correct response?
        // Pre-determine cue nature
        // if (TaskIndex == 1)
        {
            // Choose species, use the RNG from leftright
            if (leftrightposition %2 == 0)
            {
                // Duck will appear for cue and will be the correct answer
                animator.SetBool("duckyWin", true);
            }
            if (leftrightposition %2 == 1)
            {
                // Cat will appear for cue and will be the correct answer
                animator.SetBool("kittyWin", true);
            }
        }

        if (TaskIndex == 2)
        {
            // TODO: Color logic
            Animals.instance.OnTaskIndex2(animColor); // Call delegate event to change color of animals

        }




    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
