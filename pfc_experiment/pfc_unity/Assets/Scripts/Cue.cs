﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cue : StateMachineBehaviour {

    private string animTimerName = "FixationDelay";
    private Text OnScreenText;
    private Transform ducky;
    private Transform kitty;
    private GameObject[] InstructionDots;
    private MonoBehaviour monoBehavior;
    public float stateDuration = 2.0F;
    public GameObject CentralFixation;
    public GameObject dotToEnlarge;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Spawn Fixation Dot
        GameObject centerdot = GameObject.Find("CentralFixation");
        GameObject.Instantiate(CentralFixation, centerdot.transform);

        ducky = GameObject.Find("Duck").transform;
        kitty = GameObject.Find("Cat").transform;
        Vector3 cuePosition = ExperimentController.instance.cuePosition;
        Quaternion centerFacing = Quaternion.Euler(new Vector3(0.0f, -90.0f, 0.0f));

        // Spawn Enlarging Dot (We're not doing enlarging cue yet, but below is the code for enlarging dot on deltaTime)
        // GameObject enlargingdot = GameObject.Find("EnlargingDot");
        // GameObject.Instantiate(dotToEnlarge, enlargingdot.transform);

        // TODO: Cues for species and color
        if (animator.GetInteger("TaskIndex") == 1)
        {
            if (animator.GetBool("duckyWin"))
            {
                ducky.position = cuePosition;
                ducky.rotation = centerFacing;
            }
            if (animator.GetBool("kittyWin"))
            {
                kitty.position = cuePosition;
                kitty.rotation = centerFacing;
            }
        }
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        // GUI Indicator (not on VR screen)
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Giving Instruction for " + animator.GetFloat("FixationDelay").ToString("F1") + " more seconds.";

    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        updateAnimatorCueTimer(animator, 0);

        // GUI Indicator (not on VR screen)
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Loading";

        Animals.instance.MakeInvisible(true);

        // Destroy Instruction Dots 
        //InstructionDots = GameObject.FindGameObjectsWithTag("InstructionDots");
        //for (var i = 0; i < InstructionDots.Length; i++)
        //{
        //    Destroy(InstructionDots[i]);
        //}

    }

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, stateDuration - newTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
