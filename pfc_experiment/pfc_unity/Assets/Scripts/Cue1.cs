﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cue1 : StateMachineBehaviour {

    private GameObject ducky;
    private GameObject kitty;
    private int DuckTracker;
    private colorDetermine positionTracker; // contains color and position info
    private GameObject[] Fixation;
    private GameObject CentralFixation;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        // Retrieve left/right indicators
        positionTracker = GameObject.Find("ExperimentProps").GetComponent<colorDetermine>();

        // Find In-Game Objects
        ducky = GameObject.Find("Duck");
        kitty = GameObject.Find("Cat");

        // RNG-Logic for Left or Right Position for Ducky and Kitty
        if (positionTracker.leftrightposition %2 == 0)
        {
            // Left Duck Right Cat
            Animals.instance.MakeInvisible(); // Invisible = false 
            Animals.instance.SetPosition(ducky, true); // isLeft = true
            Animals.instance.SetPosition(kitty, false); 
            // Assign Position Tracker Logic
            DuckTracker--;
            animator.SetFloat("DuckTracker", DuckTracker);
        }

        if (positionTracker.leftrightposition %2 == 1)
        {
            // Right Duck Left Cat
            Animals.instance.MakeInvisible(); // Invisible = false
            Animals.instance.SetPosition(ducky, false);
            Animals.instance.SetPosition(kitty, true);
            // Assign Position Tracker Logic
            DuckTracker++;
            animator.SetFloat("DuckTracker", DuckTracker);
        }

        animator.SetTrigger("AdvanceNextStage");

    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {


    }


    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    }

}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
