﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoNoGoPresent : StateMachineBehaviour
{
    private Text OnScreenText;
    private string animTimerName = "FixationDelay";
    private Renderer fixationColor;
    private Animator animator;
    public float stateDuration = 1.0F;
    public GameObject CentralFixation;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // No-go scenario
        if (animator.GetBool("NoGoWin"))
        {
            // Flash red
            fixationColor = GameObject.Find("CentralFixation(Clone)").GetComponent<Renderer>();
            fixationColor.material.shader = Shader.Find("Specular");
            fixationColor.material.SetColor("_SpecColor", Color.red);
        }


    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Instructional cue active for " + animator.GetFloat("FixationDelay").ToString("F1") + " more seconds.";
    }

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, stateDuration - newTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        updateAnimatorCueTimer(animator, 0);

    }
}
