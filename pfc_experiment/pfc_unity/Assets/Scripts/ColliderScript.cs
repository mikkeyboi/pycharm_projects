﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour
{
    public int controllerIndex;

    // Use this for initialization
    void Start()
    {
        if (gameObject.GetComponent<SteamVR_TrackedObject>())
        {
            int index = (int)this.GetComponent<SteamVR_TrackedObject>().index;
            controllerIndex = index;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Animals")
        {
            ExperimentController.instance.colliderCubeSelect(col.gameObject); // tells colliderCubeSelect what animal is being hit
        }

    }

    void collidingFixation()
    {
        if (gameObject.GetComponent<SteamVR_TrackedObject>())
        {
            // Haptic feedback
            SteamVR_Controller.Input(controllerIndex).TriggerHapticPulse(2500);
        }
    }
}

