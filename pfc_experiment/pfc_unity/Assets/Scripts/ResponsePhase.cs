﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResponsePhase : StateMachineBehaviour
{
    private Text OnScreenText;
    private GameObject[] Fixation;
    private string animTimerName = "FixationDelay";
    public float stateDuration = 2.5F;
    public ExperimentController expControl;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Fixation = GameObject.FindGameObjectsWithTag("Fixation");
        for (var i = 0; i < Fixation.Length; i++)
        {
            Destroy(Fixation[i]);
        }
        expControl = ExperimentController.instance;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Logic for evaluating correct / incorrect responses
        int incorrectMeter = animator.GetInteger("IncorrectMeter");
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "The user is selecting the correct animal.";

        if (animator.GetBool("animalSelected"))
        {
            Debug.Log("Selecting Animal!");
            GameObject selectedAnimal = expControl.cubeIsTargeting; // Get the animal name that you targeted
            // Who should win?
            // if (animator.GetInteger("TaskIndex") == 1)  // Species task
            {

                if (animator.GetBool("duckyWin"))
                {
                    if (selectedAnimal.name == "Duck")
                    {
                        // Correct
                        expControl.CorrectResponse();
                        animator.SetTrigger("CompleteTrial");
                    }
                    if (selectedAnimal.name == "Cat")
                    {
                        // Incorrect
                        incorrectMeter++;
                        expControl.IncorrectResponse();
 
                        animator.SetTrigger("CompleteTrial");

                    }
                }
                if (animator.GetBool("kittyWin"))
                {
                    if (selectedAnimal.name == "Cat")
                    {
                        // Correct
                        expControl.CorrectResponse();
                        animator.SetTrigger("CompleteTrial");
                    }
                    if (selectedAnimal.name == "Duck")
                    {
                        // Incorrect
                        expControl.IncorrectResponse();
                        incorrectMeter++;
                        
                        animator.SetTrigger("CompleteTrial");
                    }
                }

            }
            

        }
        

        // Terminate trial if user takes too long
        if (animator.GetFloat("FixationDelay") < 0)
        {
            expControl.IncorrectResponse();
            incorrectMeter = animator.GetInteger("IncorrectMeter");
            incorrectMeter++;
            animator.SetInteger("IncorrectMeter", incorrectMeter);

            animator.SetTrigger("CompleteTrial");

        }

        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "User has " + animator.GetFloat("FixationDelay").ToString("F1") + " seconds until timeout";
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        updateAnimatorCueTimer(animator, 0);

    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, stateDuration - newTime);
    }
}
