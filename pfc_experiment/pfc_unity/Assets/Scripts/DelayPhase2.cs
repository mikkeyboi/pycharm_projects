﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelayPhase2 : StateMachineBehaviour
{

    private string animTimerName = "FixationDelay";
    private Text OnScreenText;
    public float stateDuration = 2.0F;

    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        AnimatorStateInfo animationState = animator.GetCurrentAnimatorStateInfo(0);
        updateAnimatorCueTimer(animator, animationState.normalizedTime);
        OnScreenText = GameObject.Find("OnScreenText").GetComponent<Text>();
        OnScreenText.text = "Go-no-go presents in " + animator.GetFloat("FixationDelay").ToString("F1") + " seconds.";
    }

    private void updateAnimatorCueTimer(Animator animator, float newTime)
    {
        animator.SetFloat(animTimerName, stateDuration - newTime);
    }


    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        updateAnimatorCueTimer(animator, 0);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}