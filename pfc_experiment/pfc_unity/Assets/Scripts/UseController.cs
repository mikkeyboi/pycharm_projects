﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UseController : MonoBehaviour {

    private Toggle thisToggle;

    // Use this for initialization
    void Start() {

        thisToggle = GetComponent<Toggle>();
        thisToggle.onValueChanged.AddListener(isTrue =>
        {
            onToggleChecked(isTrue);
        });

    }
	
    void onToggleChecked(bool isTrue)
    {

        if (isTrue == false)
        {
            
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
