﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enlargeScript : MonoBehaviour {
    // Scale dot size over time
    // Script is attached to the object that will be enlarged
    public GameObject dotToEnlarge;
    private Transform enlargingDot;
    private float _currentScale = InitScale;
    private const float TargetScale = 2.0f;
    private const float InitScale = 1f;
    private const int FramesCount = 100;
    private const float AnimationTimeSeconds = 2;
    private float _deltaTime = AnimationTimeSeconds / FramesCount;
    private float _dx = (TargetScale - InitScale) / FramesCount;
    private bool _upScale = true;
    private IEnumerator beginEnlargingDot()
    {
        while (true)
        {
            while (_upScale)
            {
                _currentScale += _dx;
                if (_currentScale > TargetScale)
                {
                    _upScale = false;
                    _currentScale = TargetScale;
                }
                enlargingDot = GameObject.Find("EnlargingInstruction(Clone)").GetComponent<Transform>();
                enlargingDot.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);
            }

            while (!_upScale)
            {
                _currentScale -= _dx;
                if (_currentScale < InitScale)
                {
                    _upScale = true;
                    _currentScale = InitScale;
                }
                enlargingDot = GameObject.Find("EnlargingInstruction(Clone)").GetComponent<Transform>();
                enlargingDot.localScale = Vector3.one * _currentScale;
                yield return new WaitForSeconds(_deltaTime);
            }
        }
    }

    // Use this for initialization
    void Start () {
        StartCoroutine(beginEnlargingDot());
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
