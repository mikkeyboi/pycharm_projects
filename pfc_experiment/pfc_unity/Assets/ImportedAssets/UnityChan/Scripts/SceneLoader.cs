﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

	void OnGUI()
	{
		GUI.Box(new Rect(10 , Screen.height - 100 ,100 ,90), "Change Scene");
		if(GUI.Button( new Rect(20 , Screen.height - 70 ,80, 20), "Next"))
			LoadNextScene();
		if(GUI.Button(new Rect(20 ,  Screen.height - 40 ,80, 20), "Back"))
			LoadPreScene();
	}

	void LoadPreScene()
	{
#pragma warning disable CS0618 // Type or member is obsolete
		int nextLevel = Application.loadedLevel + 1;
#pragma warning restore CS0618 // Type or member is obsolete
		if( nextLevel <= 1)
#pragma warning disable CS0618 // Type or member is obsolete
			nextLevel = Application.levelCount;
#pragma warning restore CS0618 // Type or member is obsolete

		SceneManager.LoadScene(nextLevel);
	}

	void LoadNextScene()
	{
#pragma warning disable CS0618 // Type or member is obsolete
		int nextLevel = Application.loadedLevel + 1;
#pragma warning restore CS0618 // Type or member is obsolete
#pragma warning disable CS0618 // Type or member is obsolete
		if( nextLevel >= Application.levelCount)
#pragma warning restore CS0618 // Type or member is obsolete
			nextLevel = 1;

#pragma warning disable CS0618 // Type or member is obsolete
		Application.LoadLevel(nextLevel);
#pragma warning restore CS0618 // Type or member is obsolete

	}
}
