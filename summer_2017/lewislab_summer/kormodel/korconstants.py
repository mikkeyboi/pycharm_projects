# List all constants here for the kormodel
# k['kdh'] = 28074  # uM/min
kdh = 28074 # uM/min
kmn = 100
pd = 0.8

kc1 = 238.95 # uM/(mV * min)

kc3 = 136.41 # uM/(mV * min)

kc4 = 3.600 # 1/(uM * min)
kmo = 120 # uM

ksn = 34316 # uM/min
na = 2.5

kex = 54572 # uM/min
kmadp = 3.5 # uM

kpi = 69.421 # 1/(uM * min)

kut = 781.97 # uM/min
kma = 150 # uM

klk1 = 2.500 # uM/min
klk2 = 0.037 # 1/mV

kfak = 862.10 # 1/(uM * min)
kbak = 22.747 # 1/(uM * min)

kfck = 1.9258 # 1/(uM^2 * min)
kbck = 0.00087538 # 1/(uM * min)

kef = 10000 # uM/min
ph0 = 7.0

kgl = 17.4 # 1/min
hplusrest = 0.1 # uM

kdte = 24 # uM
kdde = 347 # uM
kdti = 17 # uM
kddi = 282 # uM

rcm = 15
bn = 5

T = 298
R = 0.0083 # kJ/(mol * K)
F = 0.0965 # kJ/(mol * mV)
S = 2.303*R*T
Z = 2.303*R*T/F

u = 0.861
cbuffi = 0.022 # M * H/pH unit
cbuffe = 0.025 # M * H/pH unit

pka = 6.8
dgp0 = 31.9 # kJ/mol

emn0 = -320 # mV
emu0 = 85 # mV
emc0 = 250 # mV
ema0 = 540 # mV

o2 = 240 # uM
ct = 270 # uM
ut = 1350 # uM
nt = 2970 # uM
at = 135 # uM

mgfe = 4000 # uM
mgfi = 380 # uM

aisum = 16260 # uM
aesum = 6700 # uM
csum = 35000 # uM

## Values of independent variables , respiration rate (vc4) and AMPe at REST ## (ending with r)
vc4r = 287 # uM/min
nadhr = 1669.5 # uM
uqh2r = 1145.3 # uM
c2plusr = 53.79 # uM
o2r = 240.00 # uM
atptir = 13580 # uM
pitir = 15613 # uM
hir = 0.03536 # uM
atpter = 6693.6 # uM
adpter = 6.599 # uM
amper = 0.0182 # uM
piter = 2823.3 # uM
pcrr = 28761 # uM
her = 0.1000 # uM