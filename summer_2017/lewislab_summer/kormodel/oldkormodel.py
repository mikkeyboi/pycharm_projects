import matplotlib.pyplot as plt
import numpy as np
import korconstants as k

# import pandas as pd #datatable library
from scipy.integrate import odeint

# TODO: Evaluate paper's steady state
# TODO: Make a mind map with Cytoscape


# Define system of ODEs as function of state variables
def kormodel(state, t):
    # Unpack the state vector
    nadh = state[0]
    uqh2 = state[1]
    c2plus = state[2]
    o2 = state[3]
    hiplus = state[4]
    atpti = state[5]
    piti = state[6]
    atpte = state[7]
    adpte = state[8]
    pite = state[9]
    pcr = state[10]
    heplus = state[11]

# Algebraic Equations #

    # NADH Algebraic #

    uq = k.ut - uqh2
    phi = -np.log10(hiplus / 10 ** 6)
    phe = -np.log10(heplus / 10 ** 6)
    dph = k.Z * (phi - phe)
    dp = 1 / (1 - k.u) * dph
    emu = k.emu0 + (k.Z / 2) * np.log10(uq / uqh2)
    nadplus = k.nt - nadh
    emn = k.emn0 + k.Z/2 * np.log10(nadplus/nadh)
    dgc1 = emu - emn - dp * 4 / 2
    vdh = k.kdh * (1 / (1 + (k.kmn / (nadplus / nadh))) ** k.pd)
    vc1 = k.kc1 * dgc1

    # UQH2 Algebraic # + # C3Plus Algebraic #

    c3plus = k.ct - c2plus
    emc = k.emc0 + k.Z * np.log10((c3plus/c2plus))
    ema = emc + dp * (2 + 2 * k.u) / 2
    a3o2 = 10 ** (ema - k.ema0) / k.Z
    a2plus = k.at / (1 + a3o2)
    dgc3 = emc - emu - dp * (4-2*k.u)/2
    vc3 = k.kc3 * dgc3
    vc4 = k.kc4 * a2plus * c2plus * (1 / (1 + k.kmo / state[3]))  # vc4 = 287 # at rest


    # print(vc4, "vc4")

    # TODO: VC4 is called "Respiration Rate" and when this value changes, most of the states change
    # TODO: Should allow this value to be at 287 or customly defined, because this equation causes it to be extremely low (e-60+)

    # hiplus Algebraic # + # ATP(ti) Algebraic # + # Pi(ti) Algebraic #
    adpti = k.aisum - atpti
    dgp = k.dgp0 / k.F + k.Z * np.log10((10 ** 6 * atpti) / (adpti * piti))
    dgsn = k.na * dp - dgp
    gamma = 10 ** (dgsn / k.Z)
    vsn = k.ksn * ((gamma - 1) / (gamma + 1))
    adpfe = adpte / (1 + k.mgfe / k.kdde)
    atpfe = atpte / (1 + k.mgfe / k.kdte)
    adpfi = adpti / (1 + k.mgfi / k.kddi)
    atpfi = atpti / (1 + k.mgfi / k.kdti)
    vex = k.kex * ((adpfe / (adpfe + atpfe * 10 ** (-phi / k.Z))) - (adpfi / (adpfi + atpfi * 10 ** (-phi / k.Z)))) * (1 / (1 + k.kmadp / adpfe))
    pije = pite / (1 + 10 ** (phe - k.pka))
    piji = piti / (1 + 10 ** (phi - k.pka))
    vpi = k.kpi * (pije * heplus - piji * hiplus) ## Using He+ and Hi+ as states or resting doesn't make a difference
    vlk = k.klk1 * (np.e ** (k.klk2 * dp) - 1)
    c0i = (10 ** (-phi) - 10 ** (-phi - 0.001)) / 0.001 # TODO:  NOTE that here dph is dpH = 0.001 not delta pH from previous
    rbuffi = k.cbuffi / c0i

    # ATP(te) Algebraic
    vut = k.kut * (1 / (1 + (k.kma / atpte)))
    adpme = adpte - adpfe
    atpme = atpte - atpfe
    ampe = k.aesum - atpte - adpte
    vak = k.kfak * adpfe * adpme - k.kbak * atpme * ampe
    cr = k.csum - pcr
    vck = k.kfck * adpte * pcr * heplus - k.kbck * atpte * cr # TODO: Figure out why vck starts out negative, and why PCR starts out so high
    # print(vck, "vck")
    # print(adpte, "adpte")
    # print(pcr, "pcr")
    # print(heplus, "heplus") # way too small
    # print(atpte, "atpte") # pretty stable
    # print(cr, "cr") # constant

    vgl = k.kgl * (adpte + ampe)*(k.hplusrest/hiplus)  # <- anaerobic glycolysis present
    # vgl = 0.2 * vdh  # anaerobic glycolysis absent # ?? vOX maybe?

    # heplus Algebraic
    vef = k.kef * (k.ph0 - phe)
    c0e = (10 ** (-phi) - 10 ** (-phi - 0.001)) / 0.001 # TODO: dpH = 0.001
    rbuffe = k.cbuffe / c0e

# Differential Equations #

    # NADH Differential
    d_nadh = (vdh - vc1) * k.rcm / k.bn

    # UQH2 Differential
    d_uqh2 = (vc1 - vc3) * k.rcm

    # c2plus Differential
    d_c2plus = (vc3 - 2 * vc4) * 2 * k.rcm

    # o2 Differential
    # d_o2 = 0  # constant saturated oxygen concentration = 240 uM or O2 = -vc4
    d_o2 = - -vc4

    # hiplus Differential
    d_hiplus = - (2 * (2 + 2 * k.u) * vc4 + 4 * vc1 - k.na * vsn - k.u * vex - (1 - k.u) * vpi - vlk) * k.rcm / rbuffi

    # atpti Differential
    d_atpti = (vsn - vex) * k.rcm

    # piti Differential
    d_piti = (vpi - vsn) * k.rcm

    # ATP(te) Differential
    d_atpte = ((vex - vut + vak + vck + 1.5 * vgl) * k.rcm) / (k.rcm - 1)

    # ADP(te) Differential
    d_adpte = ((vut - vex - 2 * vak - vck - 1.5 * vgl) * k.rcm) / (k.rcm - 1) # TODO: Any inflation here - small inflations here cause massive ones elsewhere
    # TODO: Clamping this messes up the entire model
    # print(vut, "vut") - Stable @ 764
    # print(vex, "vex") - unstable -5046 -> -1396.72
    # print(vak, "vak") - unstable 32876 -> 0.00502
    # print(vgl, "vgl") - unstable 314.93 -> 1342.277
    # print(vck, "vck") - unstable -6.5709 -> 147.85


    # pite Differential
    d_pite = (vut - vpi - 1.5 * vgl) * k.rcm / (k.rcm - 1)

    # pcr Differential
    d_pcr = -vck * k.rcm / (k.rcm - 1)

    # He+ Differential
    d_heplus = (2 * (2 + 2 * k.u) * vc4 + (4 - 2 * k.u) * vc3 + 4 * vc1 - k.na * vsn - k.u * vex - (
        1 - k.u) * vpi - vlk - k.S * vck - vef + vgl - 0.2 * vdh) / (rbuffe * k.rcm / (k.rcm - 1))

    # Return the state derivatives
    return [d_nadh, d_uqh2, d_c2plus, d_o2, d_hiplus, d_atpti, d_piti, d_atpte, d_adpte, d_pite, d_pcr, d_heplus]



# Initial conditions of the state variables

# state0 = [k.nadhr, k.uqh2r, k.c2plusr, k.o2r, k.hir, k.atptir, k.pitir, k.atpter, k.adpter, k.piter, k.pcrr, k.her]
# Experiment with initial conditions
state0 = [k.nadhr, k.uqh2r, k.c2plusr, k.o2r, k.hir, k.atptir, k.pitir, k.atpter, k.adpter, k.piter, k.pcrr, k.her]


# Time vector
dt = 0.01  # time step
tmax = 12
t = np.arange(0.0, tmax, dt)

# Solve
state = odeint(kormodel, state0, t, mxstep=5000, atol=1e-10, rtol=1e-10, hmax=1e-1)

# Find VC4 to plot independently
hiplus = state[:, 4]
heplus = state[:, 11]
c2plus = state[:, 2]
phi = -np.log10(hiplus / 10 ** 6)
phe = -np.log10(heplus / 10 ** 6)
dph = k.Z * (phi - phe)
dp = 1 / (1 - k.u) * dph
c3plus = k.ct - c2plus
emc = k.emc0 + k.Z * np.log10((c3plus / c2plus))
ema = emc + dp * (2 + 2 * k.u) / 2
a3o2 = 10 ** (ema - k.ema0) / k.Z
a2plus = k.at / (1 + a3o2)
vc4 = k.kc4 * a2plus * c2plus * (1 / (1 + k.kmo / state[:,3]))  # vc4 = 287 # at rest

# Plot Solution
# ax = plt.plot(t, state) All States
ax1 = plt.plot(t, state[:, 10]) # For PCR
ax1.set_ylabel('PCR', color='r')
ax2 = plt.plot(t, vc4) # For V_c4
ax2.set_ylabel('VC4', color='b')


# Label legend
plt.legend(['NADH', 'UQH2', 'C2Plus', 'O2', 'H(i)', 'ATP(ti)', 'Pi(ti)', 'ATP(te)', 'ADP(te)', 'Pi(te)', 'PCR', 'H(e)', 'vc4'])
# ax.set_xlabel('time(s)')
# ax.set_ylabel('y')
plt.show()
