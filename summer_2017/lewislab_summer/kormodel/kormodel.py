# Size of variable arrays:
sizeAlgebraic = 46
sizeStates = 11
sizeConstants = 56

import numpy as np
import korconstants as k
from scipy.integrate import ode
from scipy.integrate import odeint
# from math import *
# import matplotlib.pyplot as plt
# import pandas as pd #datatable library



# TODO: Define constants, rates, algebraic, solver
# TODO: Anaerobic glycolysis = absent / present
# TODO: Plot 2D figures using variables in Kor. papers

def createLegends():
    legend_states = [""] * sizeStates
    legend_rates = [""] * sizeStates
    legend_algebraic = [""] * sizeAlgebraic
    legend_voi = ""
    legend_constants = [""] * sizeConstants
    legend_voi = "time in component environment (second)"
    legend_states[0] = "NADH"
    legend_states[1] = "UQH2"
    legend_states[2] = "C2+"
    legend_states[3] = "O2"
    legend_states[4] = "Hi"
    legend_states[5] = "ATP(tir)"
    legend_states[6] = "Piti"
    legend_states[7] = "ATP(ter)"
    legend_states[8] = "Piter"
    legend_states[9] = "PCR"
    legend_states[10] = "He"



# Define system of ODEs as function of state variables
def initConstants():
    constants = [0.0] * sizeConstants
    states = [0.0] * sizeStates

# Define resting state conditions
    states[0] = k.nadhr
    states[1] = k.uqh2r
    states[2] = k.c2plusr
    states[3] = k.o2r
    states[4] = k.hir
    states[5] = k.atptir
    states[6] = k.pitir
    states[7] = k.atpter
    states[8] = k.piter
    states[9] = k.pcrr
    states[10] = k.her

# Define constants
    constants[0] = k.kdh # k(DH) uM/min
    constants[1] = k.kmn # K(mN)
    constants[2] = 0.8 # P(D)
    constants[3] = 238.95 #  K(c1) uM/(mV * min)
    constants[4] = 136.41 # k(c3) uM/(mV * min)
    constants[5] = 3.600 # k(c4) 1/(uM * min)
    constants[6] = 120 # k(mO) uM
    constants[7] = 34316 # k(SN) uM/min
    constants[8] = 2.5 # n(A)
    constants[9] = 54572 # k(EX) uM/min
    constants[10] = 3.5 # k(mADP) uM
    constants[11] = 69.421 # k(PI) 1/(uM * min)
    constants[12] = 781.97 # k(UT) uM/min
    constants[13] = 150 # k(mA) uM
    constants[14] = 2.500 # k(LK1) uM/min
    constants[15] = 0.038 # k(LK2) 1/mV
    constants[16] = 862.10 # k(fAK) 1/(uM * min)
    constants[17] = 22.747 # k(bAK) 1/(uM * min)
    constants[18] = 1.9258 # k(fCK) 1/(uM^2 * min)
    constants[19] = 0.00087538 # k(bCK) 1/(uM * min)
    constants[20] = 10000 # k(EF) uM/min
    constants[21] = 7.0 # pH(0)
    constants[22] = 17.4 # k(GL) 1/min
    constants[23] = 0.1 # H+ rest uM
    constants[24] = 24 # k(DTe) uM
    constants[25] = 347 # k(DDe) uM
    constants[26] = 17 # k(DTi) uM
    constants[27] = 282 # k(DDi) uM
    constants[28] = 15 # R(Cm)
    constants[29] = 5 # B(N)
    constants[30] = 298 # T in Kelvin
    constants[31] = 0.0083 # R kJ/(mol * K)
    constants[32] = 0.0965 # F kJ/(mol * mV)
    constants[33] = 2.303*constants[31]*constants[30] # S
    constants[34] = 2.303*constants[31]*constants[30]/constants[32] # Z
    constants[35] = 0.861 # u (delta phi / delta p)
    constants[36] = 0.022 # c(buffi) M * H/pH unit
    constants[37] = 0.025 # c(buffe) M * H/pH unit
    constants[38] = 6.8 # pKa
    constants[39] = 31.9 # delta G(P0) kJ/mol
    constants[40] = -320 # E(mN0) mV
    constants[41] = 85 # E(mU0) mV
    constants[42] = 250 # E(mc0) mV
    constants[43] = 540 # E(ma0) mV
    constants[44] = 240 # O2 uM
    constants[45] = 270 # c(t) uM
    constants[46] = 1350 # U(t) uM
    constants[47] = 2970 # N(t) uM
    constants[48] = 135 # a(t) uM
    constants[49] = 4000 # Mg(fe) uM
    constants[50] = 380 # Mg(fi) uM
    constants[51] = 16260 # A(iSUM) uM
    constants[52] = 6700 # A(eSUM) uM
    constants[53] = 35000 # C(SUM) uM
    constants[54] = 287 # V(C4) # uM/min
    constants[55] = 6.599 # ADP(te) # uM
    return states, constants


def computeRates(voi, states, constants):
    rates = [0.0] * sizeStates
    algebraic = [0.0] * sizeAlgebraic

    algebraic[0] = constants[46] - states[1] # UQ = Ut - UQH2
    algebraic[1] = -np.log10(states[4] / (10 ** 6))  # pH(i)

    # If error, use at rest states ^
    algebraic[2] = -np.log10(states[10] / 10 ** 6) # pH(e)
    algebraic[3] = constants[34] * (algebraic[1] - algebraic[2]) # delta pH
    algebraic[4] = 1 / (1 - constants[35]) * algebraic[3] # delta p
    algebraic[5] = constants[41] + (constants[34] / 2) * np.log10(algebraic[0] / states[1]) # E(mU)
    algebraic[6] = constants[47] - states[0] # NAD+
    algebraic[7] = constants[40] + constants[34]/2 * np.log10(algebraic[6] / states[0]) # E(mN)
    algebraic[8] = algebraic[5] - algebraic[7] - algebraic[4] * 4 / 2 # delta G(C1)
    algebraic[9] = constants[0] * (1 / (1 + (constants[1] / (algebraic[6] / states[0]))) ** constants[2]) # V(DH)
    algebraic[10] = constants[3] * algebraic[8] # v(c1)

    # NADH Differential
    rates[0] = (algebraic[9] - algebraic[10]) * constants[28] / constants[29]

    algebraic[11] = constants[45] - states[2] # c3+
    algebraic[12] = constants[42] + constants[34] * np.log10((algebraic[11]/states[2])) # E(mc)
    algebraic[13] = algebraic[12] + algebraic[4] * (2 + 2 * constants[35]) / 2 # E(ma)
    algebraic[14] = 10 ** (algebraic[13] - constants[43]) / constants[34] # a(3/2)
    algebraic[15] = constants[48] / (1 + algebraic[14]) # a2+
    algebraic[16] = algebraic[12] - algebraic[5] - algebraic[4] * (4-2*constants[35])/2 # delta G(c3)
    algebraic[17] = constants[4] * algebraic[16] # v(c3)
    algebraic[18] = constants[5] * algebraic[15] * states[2] * (1 / (1 + constants[6] / states[3]))  # vc4 = 287 # at rest

    # c2+ Differential
    rates[1] = (algebraic[17] - 2 * algebraic[18]) * 2 * constants[28]

    # UQH2 Differential
    rates[2] = (algebraic[10] - algebraic[17]) * constants[28]

    # o2 Differential
    rates[3] = 0  # constant saturated oxygen concentration = 240 uM or O2 = -vc4

    algebraic[19] = constants[51] - states[5] # ADP(ti)
    algebraic[20] = constants[39] / constants[32] + constants[34] * np.log10(10 ** 6 * states[5] / (algebraic[19] * states[6])) # delta G(p)
    algebraic[21] = constants[8] * algebraic[4] - algebraic[20] # delta G(SN)
    algebraic[22] = 10 ** (algebraic[21] / constants[34]) # gamma
    algebraic[23] = constants[7] * ((algebraic[22] - 1) / (algebraic[22] + 1)) # V(SN)
    algebraic[24] = constants[55] / (1 + constants[49] / constants[25]) # ADP(fe)
    algebraic[25] = states[7] / (1 + constants[49] / constants[24]) # ATP(fe)
    algebraic[26] = algebraic[19] / (1 + constants[50] / constants[27]) # ADP(fi)
    algebraic[27] = states[5] / (1 + constants[50] / constants[26]) # ATP(fi)
    algebraic[28] = constants[9] * ((algebraic[24] / (algebraic[24] + algebraic[25] * 10 ** (-algebraic[1] / constants[34]))) - (algebraic[26] / (algebraic[26] + algebraic[27] * 10 ** (-algebraic[1] / constants[34])))) * (
    1 / (1 + constants[10] / algebraic[24])) # V(EX)
    algebraic[29] = states[8] / (1 + 10 ** (algebraic[2] - constants[38])) # Pi(je)
    algebraic[30] = states[6] / (1 + 10 ** (algebraic[1] - constants[38])) # Pi(ji)
    algebraic[31] = constants[11] * (algebraic[29] * states[10] - algebraic[30] * states[4]) # V(pi)
    algebraic[32] = constants[14] * (np.e ** (constants[15] * algebraic[4]) - 1) # V(LK)
    algebraic[33] = (10 ** (-algebraic[1]) - 10 ** (-algebraic[1] - algebraic[3])) / algebraic[3] # c(0i) natural buffering capacity for H+ in matrix
    algebraic[34] = constants[36] / algebraic[33] # r(buffi)

    # H(i)+ Differential
    rates[4] = - (2 * (2 + 2 * constants[35]) * algebraic[18] + 4 * algebraic[10] - constants[8] * algebraic[23] - constants[35] * algebraic[28] - (1 - constants[35]) * algebraic[31] - algebraic[32]) * constants[28] / algebraic[34]

    # atpti Differential
    rates[5] = (algebraic[23] - algebraic[28]) * constants[28]

    # piti Differential
    rates[6] = (algebraic[31] - algebraic[23]) * constants[28]


    algebraic[35] = constants[12] * (1 / (1 + (constants[13] / states[7]))) # V(UT)
    algebraic[36] = constants[55] - algebraic[24] # ADP(me)
    algebraic[37] = states[7] - algebraic[25] # ATP(me)
    algebraic[38] = constants[52] - states[7] - constants[55] # AMP(e)
    algebraic[39] = constants[16] * algebraic[24] * algebraic[36] - constants[17] * algebraic[37] * algebraic[38] # v(ak)
    algebraic[40] = constants[53] - states[9] # Cr
    algebraic[41] = constants[18] * constants[55] * states[9] * states[10] - constants[19] * states[7] * algebraic[40] # V(CK)
    algebraic[42] = constants[22] * (constants[55] + algebraic[38])*(constants[23]/constants[23])  # <- anaerobic glycolysis present V(GL)
    # v(gl) = 0.2 * vdh  # anaerobic glycolysis absent ... # h+ divided by h+ !!

    # ATP(te) Differential
    rates[7] = (algebraic[28] - algebraic[35] + algebraic[39] + algebraic[41] + 1.5 * algebraic[42]) * constants[28] / (constants[28] - 1) # ATP(te)

    # pite Differential
    rates[8] = (algebraic[35] - algebraic[31] - 1.5 * algebraic[42]) * constants[28] / (constants[28] - 1)

    # pcr Differential
    rates[9] = -algebraic[41] * constants[28] / (constants[28] - 1)

    algebraic[43] = constants[20] * (constants[21] - algebraic[2]) # V(EF)
    algebraic[44] = (10 ** (-algebraic[1]) - 10 ** (-algebraic[1] - algebraic[3])) / algebraic[3] # C(0e)
    algebraic[45] = constants[37] / algebraic[44] # r(buffe)

    # heplus Differential
    rates[10] = (2 * (2 + 2 * constants[35]) * constants[54] + (4 - 2 * constants[35]) * algebraic[17] + 4 * algebraic[10] - constants[8] * algebraic[23] - constants[35] * algebraic[28] -
                 (1 - constants[35]) * algebraic[31] - algebraic[32] - constants[33] * algebraic[41] - algebraic[43] + algebraic[42] - 0.2 * algebraic[9]) / (algebraic[45] * constants[28] / (constants[28] - 1))

    # Return the state derivatives
    # return [d_nadh, d_uqh2, d_c2plus, d_o2, d_hiplus, d_atpti, d_piti, d_atpte, d_pite, d_pcr, d_heplus]
    return rates




def computeAlgebraic(constants, states, voi):
    algebraic = np.array([[0.0] * len(voi)] * sizeAlgebraic)
    states = np.array(states)
    voi = np.array(voi)
    algebraic[0] = constants[46] - states[1] # UQ = Ut - UQH2
    print(algebraic[0])
    algebraic[1] = -np.log10(states[4] / (10 ** 6)) # pH(i)
    print(algebraic[1])
    algebraic[2] = -np.log10(states[10] / (10 ** 6)) # pH(e)
    algebraic[3] = constants[34] * (algebraic[1] - algebraic[2]) # delta pH
    algebraic[4] = 1 / (1 - constants[35]) * algebraic[3] # delta p
    algebraic[5] = constants[41] + (constants[34] / 2) * np.log10(algebraic[0] / states[1]) # E(mU)
    algebraic[6] = constants[47] - states[0] # NAD+
    algebraic[7] = constants[40] + constants[34]/2 * np.log10(algebraic[6] / states[0]) # E(mN)
    algebraic[8] = algebraic[5] - algebraic[7] - algebraic[4] * 4 / 2 # delta G(C1)
    algebraic[9] = constants[0] * (1 / (1 + (constants[1] / (algebraic[6] / states[0]))) ** constants[2]) # V(DH)
    algebraic[10] = constants[3] * algebraic[8] # v(c1)
    algebraic[11] = constants[45] - states[2] # c3+
    algebraic[12] = constants[42] + constants[34] * np.log10((algebraic[11]/states[2])) # E(mc)
    algebraic[13] = algebraic[12] + algebraic[4] * (2 + 2 * constants[35]) / 2 # E(ma)
    algebraic[14] = 10 ** (algebraic[13] - constants[43]) / constants[34] # a(3/2)
    algebraic[15] = constants[48] / (1 + algebraic[14]) # a2+
    algebraic[16] = algebraic[12] - algebraic[5] - algebraic[4] * (4-2*constants[35])/2 # delta G(c3)
    algebraic[17] = constants[4] * algebraic[16] # v(c3)
    algebraic[18] = constants[5] * algebraic[15] * states[2] * (1 / (1 + constants[6] / states[3]))  # vc4 = 287 # at rest
    algebraic[19] = constants[51] - states[5] # ADP(ti)
    algebraic[20] = constants[39] / constants[32] + constants[34] * np.log10(10 ** 6 * states[5] / (algebraic[19] * states[6])) # delta G(p)
    algebraic[21] = constants[8] * algebraic[4] - algebraic[20] # delta G(SN)
    algebraic[22] = 10 ** (algebraic[21] / constants[34]) # gamma
    algebraic[23] = constants[7] * ((algebraic[22] - 1) / (algebraic[22] + 1)) # V(SN)
    algebraic[24] = constants[55] / (1 + constants[49] / constants[25]) # ADP(fe)
    algebraic[25] = states[7] / (1 + constants[49] / constants[24]) # ATP(fe)
    algebraic[26] = algebraic[19] / (1 + constants[50] / constants[27]) # ADP(fi)
    algebraic[27] = states[5] / (1 + constants[50] / constants[26]) # ATP(fi)
    algebraic[28] = constants[9] * ((algebraic[24] / (algebraic[24] + algebraic[25] * 10 ** (-algebraic[1] / constants[34]))) - (algebraic[26] / (algebraic[26] + algebraic[27] * 10 ** (-algebraic[1] / constants[34])))) * (
    1 / (1 + constants[10] / algebraic[24])) # V(EX)
    algebraic[29] = states[8] / (1 + 10 ** (algebraic[2] - constants[38])) # Pi(je)
    algebraic[30] = states[6] / (1 + 10 ** (algebraic[1] - constants[38])) # Pi(ji)
    algebraic[31] = constants[11] * (algebraic[29] * states[10] - algebraic[30] * states[4]) # V(pi)
    algebraic[32] = constants[14] * (np.e ** (constants[15] * algebraic[4]) - 1) # V(LK)
    algebraic[33] = (10 ** (-algebraic[1]) - 10 ** (-algebraic[1] - algebraic[3])) / algebraic[3] # c(0i) natural buffering capacity for H+ in matrix
    algebraic[34] = constants[36] / algebraic[33] # r(buffi)
    algebraic[35] = constants[12] * (1 / (1 + (constants[13] / states[7]))) # V(UT)
    algebraic[36] = constants[55] - algebraic[24] # ADP(me)
    algebraic[37] = states[7] - algebraic[25] # ATP(me)
    algebraic[38] = constants[52] - states[7] - constants[55] # AMP(e)
    algebraic[39] = constants[16] * algebraic[24] * algebraic[36] - constants[17] * algebraic[37] * algebraic[38] # v(ak)
    algebraic[40] = constants[53] - states[9] # Cr
    algebraic[41] = constants[18] * constants[55] * states[9] * states[10] - k.kbck * states[7] * algebraic[40] # V(CK)
    algebraic[42] = constants[22] * (constants[55] + algebraic[38])*(constants[23]/constants[23])  # <- anaerobic glycolysis present
    # vgl = 0.2 * vdh  # anaerobic glycolysis absent
    algebraic[43] = constants[20] * (constants[21] - algebraic[2]) # V(EF)
    algebraic[44] = (10 ** (-algebraic[1]) - 10 ** (-algebraic[1] - algebraic[3])) / algebraic[3] # C(0e)
    algebraic[45] = constants[37] / algebraic[44] # r(buffe)

    return algebraic

def solve_model():
    """Solve model with ODE solver"""
    # Initialise constants and state variables
    (init_states, constants) = initConstants()

    # Set timespan to solve over
    voi = np.linspace(0, 1, 300)

    # Construct ODE object to solve
    r = ode(computeRates)
    r.set_integrator('vode', method='bdf', atol=1e-06, rtol=1e-06, max_step=0.1)
    r.set_initial_value(init_states, voi[0])
    r.set_f_params(constants)

    # Solve model
    #states = np.array([[0.0] * len(voi)] * sizeStates)
    states[0] = init_states
    for (i,t) in enumerate(voi[1:]):
        if r.successful():
            r.integrate(t)
            states[i+1] = r.y
        else:
            print('Terminating early because !r.successful()')
            break

    # Compute algebraic variables
    algebraic = computeAlgebraic(constants, states, voi)
    return voi, states, algebraic

def plot_model(voi, states, algebraic):
    """Plot variables against variable of integration"""
    import pylab
    # (legend_states, legend_algebraic, legend_voi, legend_constants) = createLegends()
    pylab.figure(1)
    pylab.plot(voi, np.vstack((states, algebraic)).T)
    # pylab.xlabel(legend_voi)
    # pylab.legend(legend_states + legend_algebraic, loc='best')
    pylab.show()

if __name__ == "__main__":
    (voi, states, algebraic) = solve_model()
    plot_model(voi, states, algebraic)
# #
# # # Time vector
# dt = 0.1  # time step
# tmax = 30
# t = np.arange(0.0, tmax, dt)
# import matplotlib.pyplot as plt
# plt.plot(t, np.vstack((states, algebraic)).T)
# plt.show()
#
# #
# # # Solve
# # state = odeint(kormodel, state0, t)


