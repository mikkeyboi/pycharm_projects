﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using UnityEngine;

public class speakerPlay : MonoBehaviour {

    // public AudioClip otherClip;
    new AudioSource audio;
    enum PlayState
    {
        STARTED,
        UNDEFINED
    }
    private PlayState myState = PlayState.UNDEFINED;

    [Tooltip("Port name with which the SerialPort object will be created.")]
    public string portName = "COM7";

    [Tooltip("Baud rate that the serial device is using to transmit data.")]
    public int baudRate = 115200;

    public string message = "test.wav";

    // Object from the .Net framework used to communicate with serial devices.
    private SerialPort serialPort;

    // Amount of milliseconds alloted to a single read or connect. An
    // exception is thrown when such operations take more than this time
    // to complete.
    private const int readTimeout = 100;

    // Amount of milliseconds alloted to a single write. An exception is thrown
    // when such operations take more than this time to complete.
    private const int writeTimeout = 100;

    private bool bPlay = false;


    void OnEnable()
    {
        serialPort = new SerialPort(portName, baudRate);
        serialPort.ReadTimeout = readTimeout;
        serialPort.WriteTimeout = writeTimeout;
        serialPort.Open();
    }

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            bPlay = !bPlay;
        }

        if (myState == PlayState.STARTED)
        {
            myState = PlayState.UNDEFINED;
            GetComponent<Camera>().backgroundColor = Color.black;
            
        }
 
        if (!audio.isPlaying && bPlay)
        {
            // audio.clip = otherClip;
            audio.Play();
            myState = PlayState.STARTED;
            GetComponent<Camera>().backgroundColor = Color.white;
            // TODO: Serial output.
            Debug.Log("Begin to play sound! " + Time.time * 1000);
        }
    }
        
    void SpamEnter()
    {
        Input.GetKeyDown(KeyCode.Keypad0);
    }

    void LateUpdate()
    {
        if (myState == PlayState.STARTED)
        {
            // TODO: Serial output.
            Debug.Log("Playback LateUpdate " + Time.time * 1000);
            serialPort.WriteLine((string) message);
        }
    }

    void OnDisable()
    {
        try
        {
            serialPort.Close();
        }
        catch (IOException)
        {
            // Nothing to do, not a big deal, don't try to cleanup any further.
        }

        serialPort = null;
    }
}
