﻿using UnityEngine;
using System.Collections;

public class CameraChangeColor : MonoBehaviour
{
    public bool click = false;

    public Color color1 = Color.black;
    public Color color2 = Color.white;
    public float duration = 3.0f;
    private float deltaTime = 0.0f;

    void Start()
    {
        GetComponent<Camera>().GetComponent<Camera>();
        color1 = Color.black;
        color2 = Color.white;
        duration = 3.0f;
        deltaTime = 0.0f;
    }

    void Update()
    {
        if (click)
        {
            deltaTime += Time.deltaTime;
            if (deltaTime < duration)
            {
                
                GetComponent<Camera>().backgroundColor = Color.Lerp(color1, color2, deltaTime);
            }
            else
            {
                Application.LoadLevel("TestScene1");
            }
        }
    }
}