# -*- coding: utf-8 -*-
"""
Example of how to extract and plot continuous data saved in Blackrock nsX data files
current version: 1.1.1 --- 07/22/2016

@author: Mitch Frankel - Blackrock Microsystems
"""

"""
Version History:
v1.0.0 - 07/05/2016 - initial release - requires brpylib v1.0.0 or higher
v1.1.0 - 07/12/2016 - addition of version checking for brpylib starting with v1.2.0
                      minor code cleanup for readability
v1.1.1 - 07/22/2016 - now uses 'samp_per_sec' as returned by NsxFile.getdata()
                      minor modifications to use close() functionality of NsxFile class
"""

import os
import array
import matplotlib.pyplot as plt
import numpy as np
from brpylib import NsxFile, NevFile, brpylib_ver


# Version control
brpylib_ver_req = "1.3.1"
if brpylib_ver.split('.') < brpylib_ver_req.split('.'):
    raise Exception("requires brpylib " + brpylib_ver_req + " or higher, please use latest version")

# Inits
datapath = os.path.abspath(os.path.join(os.getcwd(), ''))
print(datapath)
datafile = '20170703-140015-018'

elec_ids = 'all'     # 'all' is default for all (1-indexed)
start_time_s = 0     # 0 is default for all
data_time_s = 'all'  # 'all' is default for all
downsample = 1       # 1 is default
plot_chan = 129      # 1-indexed
baseline_interval = [1, 4]
threshold = [-150, -1000, -300]
audio_dur = 0.2

# Open file and extract headers
nsx_file = NsxFile(os.path.join(datapath, datafile + '.ns6'))
# Extract data - note: data will be returned based on *SORTED* elec_ids, see cont_data['elec_ids']
cont_data = nsx_file.getdata(elec_ids, start_time_s, data_time_s, downsample)
# Close the nsx file now that all data is out
nsx_file.close()
x = np.arange(cont_data['data'].shape[1], dtype=np.int32)
fs = cont_data['samp_per_s']
t = x / fs
data = cont_data['data']
chan_labels = [nsx_file.extended_headers[hdr_idx]['ElectrodeLabel']
               for hdr_idx in cont_data['ExtendedHeaderIndices']]

# Subtract baseline from all data channels
baseline_bool = np.logical_and(t >= baseline_interval[0],
                               t <= baseline_interval[1])
baseline = np.mean(data[:, baseline_bool], axis=1)
data -= baseline[:, np.newaxis]


# Load nev file
# Open file and extract headers
nev_file = NevFile(os.path.join(datapath, datafile + '.nev'))        # From brpylib
# Extract data and separate out spike data
# Note, can be simplified: spikes = nev_file.getdata(chans)['spike_events'], shown this way for general getdata() call
nev_data = nev_file.getdata(elec_ids)
# Close the nev file now that all data is out
nev_file.close()

# Find serial port events in nev data
ser_events = nev_data['dig_events']
ser_events_strings = array.array('B', ser_events['Data'][0]).tostring().decode('UTF8').splitlines()
timestamps = np.zeros((len(ser_events_strings)), dtype=np.int32)
offset = 0
for ev_ix in range(len(ser_events_strings)):
    timestamps[ev_ix] = ser_events['TimeStamps'][0][offset]
    offset += (len(ser_events_strings[ev_ix]) + 2)


# For each serial event, for each channel, find the first threshold crossing after the event
thresh_crossings = np.nan * np.zeros((data.shape[0], timestamps.size), dtype=np.int32)
for ev_ix in range(timestamps.size):
    this_timestamp = timestamps[ev_ix]
    if ev_ix < (timestamps.size - 1):
        next_timestamp = timestamps[ev_ix+1]
    else:
        next_timestamp = data.shape[1]

    for chan_ix in range(data.shape[0]):
        if threshold[chan_ix] >= 0:
            past_thresh = np.where(data[chan_ix, this_timestamp:next_timestamp] > threshold[chan_ix])[0]
        else:
            past_thresh = np.where(data[chan_ix, this_timestamp:next_timestamp] < threshold[chan_ix])[0]
        # thresh_crossings[chan_ix, ev_ix] = this_timestamp + past_thresh[0]
        if chan_labels[chan_ix] == 'ainp1':
            thresh_crossings[chan_ix, ev_ix] = this_timestamp + past_thresh[-1] - audio_dur*fs
        elif chan_labels[chan_ix] == 'ainp2':
            thresh_crossings[chan_ix, ev_ix] = this_timestamp + past_thresh[0]
        elif chan_labels[chan_ix] == 'ainp3':
            thresh_crossings[chan_ix, ev_ix] = this_timestamp + past_thresh[-1] - audio_dur*fs
thresh_crossings = thresh_crossings.astype(np.int32)

plt.rcParams['agg.path.chunksize'] = 4000000 # Otherwise error: Exceeded cell block limit
plt.subplot(3, 2, 1)
plt.plot(t, data.T)
plt.scatter(t[timestamps], np.zeros(timestamps.shape), c='r')
scatter_data = np.asarray(threshold)[:, np.newaxis] * np.ones(thresh_crossings.shape)
scatter_colours = ['b', 'orange', 'g']
for chan_ix in range(scatter_data.shape[0]):
    plt.scatter(t[thresh_crossings[chan_ix, :]], scatter_data[chan_ix, :], c=scatter_colours[chan_ix])

latencies = thresh_crossings - timestamps
for chan_ix in range(latencies.shape[0]):
    plt.subplot(3, 2, 2+chan_ix)
    plt.hist(1000*latencies[chan_ix]/fs, 50)
    plt.title(chan_labels[chan_ix])
# plt.hist((latencies[1, :] - latencies[0, :])/fs)

audio_diff = (latencies[2] - latencies[0]) / fs
plt.subplot(3, 2, 5)
plt.hist(1000*audio_diff, 50)
plt.title(chan_labels[2] + '-' + chan_labels[0])

shortest_diff_ix = np.argmin(audio_diff[1:]) + 1
print('Shortest audio diff at ' + str(timestamps[shortest_diff_ix] / fs))


print('Longest audio delay at ' + str(timestamps[np.argmax(latencies[0])]/fs))

plt.show()


temp = np.zeros((1+data.shape[0], t.size))
temp[0, timestamps] = 1
for chan_ix in range(data.shape[0]):
    temp[chan_ix, thresh_crossings[chan_ix, :]] = 2 + chan_ix


# Plot the data channel
ch_idx  = cont_data['elec_ids'].index(plot_chan)
hdr_idx = cont_data['ExtendedHeaderIndices'][ch_idx]
chan_labels = nsx_file.extended_headers[hdr_idx]['ElectrodeLabel']


plt.plot(t/30000, cont_data['data'][ch_idx])
plt.axis([t[0], t[-1]/30000, min(cont_data['data'][ch_idx]), max(cont_data['data'][ch_idx])])
plt.locator_params(axis='y', nbins=20)
plt.xlabel('Time (s)')
plt.ylabel("Output (" + nsx_file.extended_headers[hdr_idx]['Units'] + ")")
plt.title(nsx_file.extended_headers[hdr_idx]['ElectrodeLabel'])
plt.show()
