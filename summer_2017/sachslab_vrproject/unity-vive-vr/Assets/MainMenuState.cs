﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuState : StateMachineBehaviour {

    private Canvas canvas;
    private int initialFrame;
    private TextEditor frameCount;

    public interface IStateTransition
    {
        /// Exit the current state over time. This function should 'yield return' until the exit process
        /// is finished.
        IEnumerable Exit();

        /// Enter the next state over time. This function should 'yield return' until the enter process
        /// is finished.
        IEnumerable Enter();
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Create the main menu UI
        var canvasPrefab = Resources.Load<Canvas>("MainMenu");
        canvas = UnityEngine.Object.Instantiate(canvasPrefab);
        var playButtonGO = canvas.transform.Find("PlayButton");
        var playButton = playButtonGO.GetComponent<Button>();
        playButton.onClick.AddListener(HandlePlayButton);
        var frameCountGO = canvas.transform.Find("FrameCount");
        frameCount = frameCountGO.GetComponent<TextEditor>();
        initialFrame = Time.frameCount;
    }

    public IEnumerable Execute()
    {
        while (true)
        {
            var numFrames = Time.frameCount - initialFrame;
            frameCount.text = "Frames spent on menu: " + numFrames;
            yield return null;
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Clean up UI
        Destroy(canvas.gameObject);
    }

    public void HandlePlayButton()
    {
        // Transition to the play state using a screen fade
        var nextState = new PlayState();

    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
