﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFadeTransition : StateMachineBehaviour
{
	private Canvas canvas;
    private Image cover;
    private float fadeTime;


    /// Create the transition

    /// <param name="fadeTime">Time in seconds to complete both parts of the phase</param>

    public ScreenFadeTransition(float fadeTime)
    {
        // Set up the fade cover
        var screenFadePrefab = Resources.Load<Canvas>("ScreenFade");
        canvas = Instantiate(screenFadePrefab);
        var coverGO = canvas.transform.Find("Cover");
        cover = coverGO.GetComponent<Image>();
        this.fadeTime = fadeTime;
    }

    public IEnumerable Exit()
    {
        // Fade out
        foreach (var e in TweenAlpha(0, 1, fadeTime / 2))
        {
            yield return e;
        }
    }

    public IEnumerable Enter()
    {
        // Fade in
        foreach (var e in TweenAlpha(1, 0, fadeTime / 2))
        {
            yield return e;
        }

        // Clean up the fade cover
        UnityEngine.Object.Destroy(canvas.gameObject);
    }

    // Tween the alpha of the fade cover
    private IEnumerable TweenAlpha(
        float fromAlpha,
        float toAlpha,
        float duration
    )
    {
        var startTime = Time.time;
        var endTime = startTime + duration;
        while (Time.time < endTime)
        {
            var sinceStart = Time.time - startTime;
            var percent = sinceStart / duration;
            var color = cover.color;
            color.a = Mathf.Lerp(fromAlpha, toAlpha, percent);
            cover.color = color;
            yield return null;
        }
    }
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
