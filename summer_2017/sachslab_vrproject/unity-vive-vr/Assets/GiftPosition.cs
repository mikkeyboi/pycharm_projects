﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class GiftPosition : MonoBehaviour {

    // CASE STIM2: User has control
    //CASE FEEDBACK: No control, uses gravity
    //ALL OTHER CASES: no control, finds home target

    public VRNode hand;
    public FindTargetTaskController taskController;
    private bool userHasControl;
    private bool done;
    public Rigidbody rb;

    void Start () {
        if (hand == 0)
        {
            hand = VRNode.LeftHand;
        }
        if (taskController == null)
        {
            taskController = GameObject.Find("TaskController").GetComponent<FindTargetTaskController>();
        }
        //rb = GetComponent<Rigidbody>();
        userHasControl = true;
    }
	
	// Update is called once per frame
	void Update () {
        if(userHasControl)
        {
            transform.position = InputTracking.GetLocalPosition(hand);
            transform.rotation = InputTracking.GetLocalRotation(hand);
            GetComponent<Rigidbody>().useGravity = false;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public void setControl(bool control)
    {
        if (userHasControl != control)
        {
            userHasControl = control;
            GetComponent<Rigidbody>().useGravity = !GetComponent<Rigidbody>().useGravity;
            GetComponent<Rigidbody>().isKinematic = !GetComponent<Rigidbody>().isKinematic;
        }
        
    }
}
