﻿using UnityEngine;
using System.Collections;

public class BeltPosition : MonoBehaviour
{

    public GameObject target;
    public GameObject plane;
    public bool snapOnStart = true;
    public bool trackContinuously = false;
    public float moveSpeed = 0.5f;
    public enum OrientEnum { LookAtTarget, LookSameAsTarget };
    public OrientEnum orientOption;

    [Header("Offset")]
    public Vector3 positionOffset = Vector3.zero;
    public Vector3 orientationOffset = Vector3.zero;

    [Header("TODO: Constraints")]
    public bool constrainPitch = false;
    public bool constrainYaw = false;
    public bool constrainRoll = false;

    [Header("Position")]
    public float forward = 0f;
    public float left = 0f;
    public float up = 0f;

    void Start()
    {

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("MainCamera");
        }
        if(plane == null)
        {
            plane = GameObject.FindGameObjectWithTag("TargetField");
        }

        if (snapOnStart)
        {
            blendToTarget(1.0f);
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Submit") || Input.GetKeyDown("space"))
        {
            blendToTarget(1.0f);
        }

        if (trackContinuously)
        {
            blendToTarget(Time.deltaTime * moveSpeed);
        }
        
    }

    private void blendToTarget(float blendWeight)
    {
        //Position
        //Vector3 destinationPosition = target.transform.position + target.transform.localRotation * positionOffset;
        Vector3 destinationPosition = new Vector3(target.transform.position.x,
            this.transform.position.y,
            target.transform.position.z);
        transform.position = Vector3.Lerp(transform.position, destinationPosition, blendWeight);
        //TODO: Figure out why when the belt is first set up the direction of moving objects is fine, but after resetting the belt with space bar, it maintains the previous direction vector.
        //Orientation
        switch (orientOption)
        {
            case OrientEnum.LookAtTarget:
                transform.LookAt(target.transform.position);
                break;
            case OrientEnum.LookSameAsTarget:
                if(constrainRoll || constrainPitch || constrainYaw)
                {
                    float rollAngle = target.transform.rotation.eulerAngles.x;
                    float yawAngle = target.transform.rotation.eulerAngles.y;
                    float pitchAngle = target.transform.rotation.eulerAngles.z;
                    if (constrainRoll)
                    {
                        rollAngle = this.transform.rotation.eulerAngles.x;
                    }
                    if (constrainYaw)
                    {
                        pitchAngle = this.transform.rotation.eulerAngles.y;
                    }
                    if (constrainPitch)
                    {
                        pitchAngle = this.transform.rotation.eulerAngles.z;
                    }
                    Vector3 targetRotation = new Vector3(rollAngle, yawAngle, pitchAngle);
                    transform.eulerAngles = targetRotation;
                }
                else
                {
                    transform.rotation = target.transform.rotation;
                }
                break;
        }

        // Add rotation offsets.
        transform.localRotation = Quaternion.Euler(orientationOffset) * transform.localRotation;
        transform.Translate(Vector3.forward * forward);
        transform.Translate(Vector3.left * left);

        //transform.rotation = Quaternion.AngleAxis(90.0f, transform.right) * transform.rotation;
        //transform.rotation = Quaternion.AngleAxis(180.0f, transform.up) * transform.rotation;
    }
}
