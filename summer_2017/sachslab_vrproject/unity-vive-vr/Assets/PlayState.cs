﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayState : StateMachineBehaviour
{
    private GameObject targetsContainer;
    private List<GameObject> targets;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Set up the targets
        targetsContainer = new GameObject("TargetsContainer");
        var targetPrefab = Resources.Load<GameObject>("Target");
        targets = new List<GameObject>(3);
        for (var i = 0; i < targets.Capacity; ++i)
        {
            var target = UnityEngine.Object.Instantiate(targetPrefab);
            target.transform.parent = targetsContainer.transform;
            target.transform.position += new Vector3(i * 2, 0, 0);
            targets.Add(target);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (var target in targets)
        {
            // Turn targets green, ready to be clicked
            SetTargetColor(target, Color.green);
        }
    }

    public IEnumerable Execute()
    {
        // Handle clicks
        while (true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                HandleClick();
            }
            yield return null;
        }
    }

   

    private void HandleClick()
    {
        // Cast a ray where the user clicked
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo) == false)
        {
            return;
        }

        // Check if the user clicked any targets
        foreach (var target in targets)
        {
            if (target != null && hitInfo.transform == target.transform)
            {
                HitTarget(target);
                break;
            }
        }
    }

    private void SetTargetColor(GameObject target, Color color)
    {
        var renderer = target.GetComponent<Renderer>();
        renderer.material.color = color;
    }

    private void HitTarget(GameObject target)
    {
        // Turn the target red so the user knows they hit it
        SetTargetColor(target, Color.red);

        // If the user hit all the targets
        targets.Remove(target);
        if (targets.Count == 0)
        {
            // Transition to the main menu using the screen fade transition
            var nextState = new MainMenuState();
            var transition = new ScreenFadeTransition(2);
            var eventArgs = new StateBeginExitEventArgs(nextState, transition);
            OnStateExit(this, eventArgs);
        }
    }

    private void OnStateExit(PlayState playState, StateBeginExitEventArgs eventArgs)
    {
        throw new NotImplementedException();
    }
}
