﻿internal class StateBeginExitEventArgs
{
    private MainMenuState nextState;
    private ScreenFadeTransition transition;

    public StateBeginExitEventArgs(MainMenuState nextState, ScreenFadeTransition transition)
    {
        this.nextState = nextState;
        this.transition = transition;
    }
}