﻿using UnityEngine;
using System.Collections;

public class FollowTarget : MonoBehaviour {

    public FindTargetTaskController taskController;
    public GameObject target;
    public bool snapOnStart = true;
    public bool trackContinuously = false;
    public float moveSpeed = 0.5f;
    public enum OrientEnum { LookAtTarget, LookSameAsTarget };
    public OrientEnum orientOption;

    [Header("Offset")]
    public Vector3 positionOffset = Vector3.zero;
    public Vector3 orientationOffset = Vector3.zero;

    [Header("TODO: Constraints")]
    public bool constrainPitch = false;
    public bool constrainYaw = false;
    public bool constrainRoll = false;

    void Start () {

        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("MainCamera");
        }
        if(taskController == null)
        {
        //     taskController = GameObject.Find("TaskController").GetComponent<FindTargetTaskController>();
        }

        if (snapOnStart)
        {
            blendToTarget(1.0f);
        }
    }
	
	void Update () {
        if (Input.GetButtonDown("Submit") || Input.GetKeyDown("space"))
        {
            blendToTarget(1.0f);
            taskController.pushSample("040001(" + transform.position.x + "," + transform.position.y + "," + transform.position.z + ")("
                + transform.rotation.x + "," + transform.rotation.y + "," + transform.rotation.z + "," + transform.rotation.w + ")");
        }

        if (trackContinuously)
        {
            blendToTarget(Time.deltaTime * moveSpeed);
        }
    }

    private void blendToTarget(float blendWeight)
    {
        Vector3 destinationPosition = target.transform.position + target.transform.localRotation * positionOffset;
        transform.position = Vector3.Lerp(transform.position, destinationPosition, blendWeight);

        //TODO: Use constraints
        
        switch (orientOption)
        {
            case OrientEnum.LookAtTarget:
                transform.LookAt(target.transform.position);
                break;
            case OrientEnum.LookSameAsTarget:
                transform.rotation = target.transform.rotation;
                break;
        }
        
        // Add rotation offsets.
        transform.localRotation = Quaternion.Euler(orientationOffset) * transform.localRotation;

        //transform.rotation = Quaternion.AngleAxis(90.0f, transform.right) * transform.rotation;
        //transform.rotation = Quaternion.AngleAxis(180.0f, transform.up) * transform.rotation;
    }
}
