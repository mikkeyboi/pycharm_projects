﻿using UnityEngine;
using System.Collections;

public class FeedbackColorChange : MonoBehaviour {
	
	public Color startColor = Color.blue;
	public Color endColor = Color.yellow;
	public int colorChannel = 0;
	public GameObject taskController;
	private Renderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        rend.material.color = startColor;

		//Register with the Inlet's OnSamplePulled event.
		if (taskController == null) {
			taskController = GameObject.Find ("TaskController");
		}
		taskController.GetComponent<CustomInlet>().OnSamplePulled += updateColor;
    }
	
	// Update is called once per frame
	void Update () {
    }

    public void updateColor(float[] sample)
    {
		float processedSig = sample[colorChannel] / 2.0f + 0.5f;
		rend.material.color = Color.LerpUnclamped(startColor, endColor, processedSig);
    }
}
