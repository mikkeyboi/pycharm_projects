﻿using UnityEngine;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.VR;
using System.IO;
using NetMQ;
using NetMQ.Sockets;
using MsgPack.Serialization;


public class ExperimenterMenuMichaelEdit : MonoBehaviour {

    public bool showMenu = true;
	public string title = "SachsLab";
	public int defaultTaskIndex = 0;
    public Vector2 menuOffset = new Vector2(10, 10);
	public Vector3[] cameraPositions; //in-editor defined camera positions

    // Maintain a reference to the VR cameraRig.
    // Necessary for HMD-controlled or controller-controlled camera. (in-progress)
	// TODO: Eliminate cameraRig in Hierarchy. Use Unity native VR calls.
    //private GameObject mainCamera;

    private int lastCameraIndex = 0;

    // For selecting the task.
    private string[] taskNames;
    private int guiTaskIndex = 0;
    private int loadedTaskIndex = -1;
    public event PupilGazeTracker.OnCalibrationStartedDeleg OnCalibrationStarted;
    public event PupilGazeTracker.OnCalibrationDoneDeleg OnCalibrationDone;
    public PupilGazeTracker.EStatus m_status = PupilGazeTracker.EStatus.Idle;
    public PupilGazeTracker.CustomInspector customInspector = new PupilGazeTracker.CustomInspector();

    Process serviceProcess;

    [HideInInspector]
    public string PupilServicePath = "";
    [HideInInspector]
    public string PupilServiceFileName = "";
    [HideInInspector]
    public PupilGazeTracker.Platform[] Platforms;
    [HideInInspector]
    public Dictionary<RuntimePlatform, string[]> PlatformsDictionary;
    [HideInInspector]
    public CalibPoints _calibPoints;

    List<Dictionary<string, object>> _calibrationData = new List<Dictionary<string, object>>();

    RequestSocket _requestSocket;

    int _calibSamples;
    int _currCalibPoint = 0;
    int _currCalibSamples = 0;

    // Use this for initialization
    void Start () {
        /* TODO: User Unity native VR library to get Nodes.
		if (VRSettings.isDeviceActive) {
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
		}
        */

        // If no in-editor camera positions were provided then use some defaults.
        if (cameraPositions.Length == 0) {
			cameraPositions = new Vector3[] {
				new Vector3(0, 0, -1),
				new Vector3(-1, 2, -1),
				new Vector3(-1, 2, 1),
				new Vector3(1, 2, -1),
				new Vector3(1, 2, 1)
			};
		}
        
        // Place the camera in the default (0th) position.
        SetCameraIndex(lastCameraIndex);

        taskNames = ReadtaskNames();
		if (SceneManager.sceneCount > 1) {
			// If more than 1 scene is already loaded (probably in Editor),
			// take not that it is loaded.
			loadedTaskIndex = System.Array.IndexOf(taskNames, SceneManager.GetSceneAt(1).name);
		}
		else if (defaultTaskIndex >= 0)
        {
            LoadTask(defaultTaskIndex);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnGUI ()
    {
        showMenu = GUI.Toggle(new Rect(menuOffset[0], menuOffset[1], 100, 25), showMenu, "Show Menu");

        if (showMenu)
        {
            GUI.Box(new Rect(menuOffset[0], menuOffset[1] + 25, 210, 180), title);
        }

        GUITaskSelector();  //one row; height = 25

        GUIShowCameraSelector();

        //TODO: LSL menus

        if (showMenu)
        {
            // Make the Options button.
			/*if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 100, 210, 25), "Options"))
            {
            }*/

            if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 100, 210, 25), "Start Trials")) //temporary
            {
                int x = UnityEngine.Random.Range(0, 2);

                if(x == 0)
                {
                    LoadTask(2);
                }
                else if (x == 1)
                {
                    LoadTask(3);
                }
            }

            // Make the Start Pupil Labs Button
            if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 125, 210, 25), "Start Pupil Services"))
            {
                RunServiceAtPath();
            }

            // Make the Pupil Labs Calibration Button
            if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 150, 210, 25), "Pupil Calibrate"))
            {
                Application.Quit();
            }

            // Make the Quit button.
            if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 175, 210, 25), "Quit"))
            {
                Application.Quit();
            }
        }
    }

    void GUITaskSelector()
    {
        if (showMenu)
        {
            // Cycle through list of tasks.
			if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 50, 25, 25), "<"))
            {
                guiTaskIndex--;
            }
            if (GUI.Button(new Rect(menuOffset[0] + 25, menuOffset[1] + 50, 25, 25), ">"))
            {
                guiTaskIndex++;
            }
            if (guiTaskIndex < 0)
            {
                guiTaskIndex = taskNames.Length - 1;
            }
            else if (guiTaskIndex >= taskNames.Length)
            {
                guiTaskIndex = 0;
            }

			GUI.Label(new Rect(menuOffset[0] + 60, menuOffset[1] + 50, 100, 25), taskNames[guiTaskIndex]);
            if (guiTaskIndex != loadedTaskIndex)
            {
                if (GUI.Button(new Rect(menuOffset[0] + 160, menuOffset[1] + 50, 50, 25), "Go!"))
                {
                    LoadTask(guiTaskIndex);
                }
            }
        }
    }

    void LoadTask(int newTaskIndex)
    {
        if (loadedTaskIndex != -1)
        {
            SceneManager.UnloadSceneAsync(taskNames[loadedTaskIndex]);
        }
        UnityEngine.Debug.Log(newTaskIndex);
        SceneManager.LoadScene(taskNames[newTaskIndex], LoadSceneMode.Additive);
        loadedTaskIndex = newTaskIndex;
    }

    void GUIShowCameraSelector()
    {
        if (showMenu)
        {
            // Cycle through list of camera positions.
            int newCameraIndex = lastCameraIndex;
            if (GUI.Button(new Rect(menuOffset[0], menuOffset[1] + 75, 25, 25), "<"))
            {
                newCameraIndex--;
            }
            if (GUI.Button(new Rect(menuOffset[0] + 25, menuOffset[1] + 75, 25, 25), ">"))
            {
                newCameraIndex++;
            }
            if (newCameraIndex != lastCameraIndex)
            {
                SetCameraIndex(newCameraIndex);
            }
            string camIndexString;
            if (lastCameraIndex == cameraPositions.Length)
            {
                camIndexString = "Controller";
            }
            else
            {
                camIndexString = lastCameraIndex.ToString();
            }
            GUI.Label(new Rect(menuOffset[0] + 60, menuOffset[1] + 75, 150, 25), "Camera Position: " + camIndexString);
        }
    }

    private static string[] ReadtaskNames()
    {
        List<string> temp = new List<string>();
        
        for (int sc_ix=0; sc_ix < SceneManager.sceneCountInBuildSettings; sc_ix++)
        {
            string name = SceneUtility.GetScenePathByBuildIndex(sc_ix);
            name = name.Substring(name.LastIndexOf('/') + 1); // only the scene name.
            name = name.Substring(0, name.Length - 6); // trim trailing .unity
            if (name.Substring(0, 4).Equals("Task"))
            {
                temp.Add(name);
            }
        }
        
        return temp.ToArray();
    }

    void SetCameraIndex(int index)
    {
        if (index < 0)
        {
            index = cameraPositions.Length - 1;
        }
        if (index > cameraPositions.Length - 1)
        {
            index = 0;
        }
        
        gameObject.transform.position = cameraPositions[index];
		Vector3 targetPos = Vector3.zero;
		if (VRSettings.isDeviceActive) {
			//TODO: targetPos = position of HMD. Use Unity native VR.
		}
		gameObject.transform.LookAt(targetPos);
        lastCameraIndex = index;
    }

    public void RunServiceAtPath()
    {
        AdjustPath();
        string servicePath = PupilServicePath;
        if (File.Exists(servicePath))
        {
            if (Process.GetProcessesByName("pupil_capture").Length > 0)
            {
                UnityEngine.Debug.LogWarning(" Pupil Capture is already running ! ");
            }
            else
            {
                serviceProcess = new Process();
                serviceProcess.StartInfo.Arguments = servicePath;
                serviceProcess.StartInfo.FileName = servicePath;
                if (File.Exists(servicePath))
                {
                    serviceProcess.Start();
                }
                else
                {
                    print("Pupil Service could not start! There is a problem with the file path. The file does not exist at given path");
                }
            }
        }
        else
        {
            if (PupilServiceFileName == "")
            {
                print("Pupil Service filename is not specified, most likely you will have to check if you have it set for the current platform under settings Platforms(DEV opt.)");
            }
        }
    }

    //Check platform dependent path for pupil service, only if there is no custom PupilServicePathSet
    public void AdjustPath()
    {
        InitializePlatformsDictionary();
        if (PupilServicePath == "" && PlatformsDictionary.ContainsKey(Application.platform))
        {
            PupilServicePath = PlatformsDictionary[Application.platform][0];
            PupilServiceFileName = PlatformsDictionary[Application.platform][1];
            print("Pupil service path is set to the default : " + PupilServicePath);
        }
        else if (!PlatformsDictionary.ContainsKey(Application.platform))
        {
            print("There is no platform default path set for " + Application.platform + ". Please set it under Settings/Platforms!");
        }

    }

    public void InitializePlatformsDictionary()
    {
        PlatformsDictionary = new Dictionary<RuntimePlatform, string[]>();
        foreach (PupilGazeTracker.Platform p in Platforms)
        {
            PlatformsDictionary.Add(p.platform, new string[] { p.DefaultPath, p.FileName });
        }
    }

    public void StartCalibration()
    {

        CalibrationGL.SetMode(PupilGazeTracker.EStatus.Calibration);

        PupilGazeTracker.CalibModeDetails _currCalibModeDetails = CalibrationModes[CurrentCalibrationMode];
        _sendRequestMessage(new Dictionary<string, object> { { "subject", "start_plugin" }, { "name", _currCalibModeDetails.calibPlugin } });
        _sendRequestMessage(new Dictionary<string, object> { { "subject", "calibration.should_start" }, { "hmd_video_frame_size", new float[] { 1000, 1000 } }, { "outlier_threshold", 35 } });
        _setStatus(PupilGazeTracker.EStatus.Calibration);

        if (OnCalibrationStarted != null)
            OnCalibrationStarted(null);

    }
    public void StopCalibration()
    {
        _setStatus(PupilGazeTracker.EStatus.ProcessingGaze);
        print("Calibration Stopping !");
        _sendRequestMessage(new Dictionary<string, object> { { "subject", "calibration.should_stop" } });
        if (OnCalibrationDone != null)
            OnCalibrationDone(null);

    }

    public Dictionary<PupilGazeTracker.CalibModes, PupilGazeTracker.CalibModeDetails> CalibrationModes
    {
        get
        {
            Dictionary<PupilGazeTracker.CalibModes, PupilGazeTracker.CalibModeDetails> _calibModes = new Dictionary<PupilGazeTracker.CalibModes, PupilGazeTracker.CalibModeDetails>();
            _calibModes.Add(PupilGazeTracker.CalibModes._2D, new PupilGazeTracker.CalibModeDetails()
            {
                name = "2D",
                calibrationPoints = _calibPoints.list2D,
                calibPlugin = "HMD_Calibration",
                positionKey = "norm_pos",
                ref_data = new double[] { 0.0, 0.0 },
                depth = 0.1f
            });
            _calibModes.Add(PupilGazeTracker.CalibModes._3D, new PupilGazeTracker.CalibModeDetails()
            {
                name = "3D",
                calibrationPoints = _calibPoints.list3D, //Get3DList(_convert: true),
                calibPlugin = "HMD_Calibration_3D",
                positionKey = "mm_pos",
                ref_data = new double[] { 0.0, 0.0, 0.0 },
                depth = 100
            });
            return _calibModes;
        }
    }

    void _setStatus(PupilGazeTracker.EStatus st)
    {
        if (st == PupilGazeTracker.EStatus.Calibration)
        {
            _calibrationData.Clear();
            _currCalibPoint = 0;
            _currCalibSamples = 0;
        }
        CalibrationGL.SetMode(st);
        m_status = st;
    }

    NetMQMessage _sendRequestMessage(Dictionary<string, object> data)
    {
        //if (Pupil.processStatus.eyeProcess0 || Pupil.processStatus.eyeProcess1) {//only allow to send/receive messages when at least one camera is connected and functioning
        NetMQMessage m = new NetMQMessage();
        m.Append("notify." + data["subject"]);

        using (var byteStream = new MemoryStream())
        {
            var ctx = new SerializationContext();
            ctx.CompatibilityOptions.PackerCompatibilityOptions = MsgPack.PackerCompatibilityOptions.None;
            var ser = MessagePackSerializer.Get<object>(ctx);
            ser.Pack(byteStream, data);
            m.Append(byteStream.ToArray());
        }

        _requestSocket.SendMultipartMessage(m);

        NetMQMessage recievedMsg;
        recievedMsg = _requestSocket.ReceiveMultipartMessage();

        return recievedMsg;
        //} else {
        //} return new NetMQMessage ();
    }

    public PupilGazeTracker.CalibModes CurrentCalibrationMode
    {
        get
        {
            if (customInspector.calibrationMode == 0)
            {
                return PupilGazeTracker.CalibModes._2D;
            }
            else
            {
                return PupilGazeTracker.CalibModes._3D;
            }
        }
    }
}


