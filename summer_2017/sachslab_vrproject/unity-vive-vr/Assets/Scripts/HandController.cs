﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class HandController : MonoBehaviour {

    public VRNode hand;

    // Use this for initialization
    void Start () {
        if (hand == 0)
        {
            hand = VRNode.LeftHand;
        }
        Color color = GetComponent<Renderer>().material.color;
        color.a = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = InputTracking.GetLocalPosition(hand);
	}
}
