﻿using UnityEngine;
using System.Collections;
using LSL;

public class TargetField : MonoBehaviour {

    public GameObject TargetPrefab;
    public Vector3[] targetRelativePositions;
    public float targetSpacing = 0.3f;
    public int constrainHomeTargetId = 0;
    private int currentHomeTargetId = -1;
    //public bool movingHome;

    //public GameObject startTarget;
    private GameObject[] targets;
    private Target at; // Reference to activated Target script
    private int atID;
    public FindTargetTaskController taskController;

    //LSL Material for initializing XML Header.
    public liblsl.XMLElement task;
    public liblsl.XMLElement targetsXML;
    // Use this for initialization
    private void Awake()
    {
        task = new liblsl.XMLElement();
        targetsXML = task.append_child("Target Field");
        targetsXML.append_child_value("Target Spacing", targetSpacing.ToString());

        if (targetRelativePositions.Length < 1)
        {
            targetRelativePositions = new Vector3[]
            {
                new Vector3(0, 0, 0),
                new Vector3(1, 0, 0),
                new Vector3(-1, 0, 0)
            };
            if (constrainHomeTargetId < 0)
            {
                currentHomeTargetId = 0;
            }
        }

        for (int tid = 0; tid<targetRelativePositions.Length; tid++)
        {
            liblsl.XMLElement targetXML = targetsXML.append_child_value("TargetID", tid.ToString());
            targetXML.append_child_value("Position", (targetRelativePositions[tid] * targetSpacing).ToString());
        }

    }
    void Start () {

        if (taskController == null)
        {
            taskController = GameObject.Find("TaskController").GetComponent<FindTargetTaskController>();
        }
        
        atID = 0;
        createTargetField();
    }

    void createTargetField ()
    {
        targets = new GameObject[targetRelativePositions.Length];
        for (int tid=0; tid<targets.Length; ++tid)
        {
            targets[tid] = (GameObject)Instantiate(TargetPrefab);
            targets[tid].transform.parent = this.transform;
            targets[tid].transform.localPosition = targetRelativePositions[tid] * targetSpacing;
        }

        if (currentHomeTargetId < 0)
        {
            if (constrainHomeTargetId >= 0)
            {
                currentHomeTargetId = constrainHomeTargetId;
            }
            else
            {
                currentHomeTargetId = 0;
            }
        }
        targets[currentHomeTargetId].GetComponent<Target>().setHome(true);
        
        taskController.pushSample("020000(" + transform.position.x + "," + transform.position.y + "," + transform.position.z + ")("
            + transform.rotation.x + "," + transform.rotation.y + "," + transform.rotation.z + "," + transform.rotation.w + ")");

        taskController.pushSample("020002" + currentHomeTargetId);
    }

    // Update is called once per frame
    void Update () {

    }

    public GameObject getRandomTarget ()
    {
        int newTargId = Random.Range(0, targets.Length);
        if (newTargId == currentHomeTargetId)
        {
            newTargId = (newTargId+1)%targets.Length; //gets the next target up
        }
        GameObject activatedTarget = targets[newTargId];
        at = activatedTarget.GetComponent<Target>();
        at.setActive(true);
        taskController.pushSample("020002"+ newTargId);
        atID = newTargId;
        return activatedTarget;
    }

    public int getActiveTargetID()
    {
        return atID;
    }

    public void clearTarget ()
    {
        if(at)
        {
            at.setActive(false);
            atID = 0;
            taskController.pushSample("020003");
        }
    }

    public liblsl.XMLElement getHeaderData()
    {
        return task;
    }
}
