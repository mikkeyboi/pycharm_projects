﻿using UnityEngine;
using System.Collections;

/* In the context of Neuro Target Practice Game, this class
 * contains the logic for the instantiated Target for a 
 * given trial with which the user will interact upon
 * encountering a particular set of stimuli.
 * Author: Anita Popescu
 * Sachs Lab, 2016.
 */

public class Target : MonoBehaviour {

    //TODO: Instead of using the gameController, trigger an event.
    //The gameController will subscribe to the events when it creates the targets.
    public FindTargetTaskController taskController;

    enum Stage { standby, home, target };

    Stage targetState = Stage.standby;

    // Use this for initialization
    void Start () {
        if (taskController == null)
        {
            taskController = GameObject.Find("TaskController").GetComponent<FindTargetTaskController>();
        }
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void rotate()
    {
        transform.Rotate(Vector3.back);
        //taskController.pushSample("Active target presented warning cue (first stimulus).");
    }

    public void setHome(bool toggle)
    {
        if(toggle)
        {
            targetState = Stage.home;
        }
        else
        {
            targetState = Stage.standby;
        }
    }

    public void setActive(bool activeState)
    {
        if(activeState)
        {
            targetState = Stage.target;
        }
        else
        {
            targetState = Stage.standby;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // TODO: Consider triggering a custom event: myCollisionEvent(this, bool isTarget)
        // TaskController will subscribe to all targets' myCollisionEvent;
        // May subscribe twice: 1 for state machine; 1 for LSL Outlet
        taskController.pushSample("030001" + targetState);
        if (targetState == Stage.target)
        {
            taskController.TargetSelected();
        }
        else if (targetState == Stage.home)
        {
            taskController.setIsHome(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (targetState == Stage.home)
        {
            taskController.setIsHome(false);
        }
        taskController.pushSample("030002" + targetState);
    }
}
