﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackTextChange : MonoBehaviour {

	public int valueChannel = 0;
	public GameObject taskController;
	private bool wasPositive = false;
	private Text feedbackText;

	// Use this for initialization
	void Start () {
		//Register with the Inlet's OnSamplePulled event.
		if (taskController == null) {
			taskController = GameObject.Find ("TaskController");
		}
		taskController.GetComponent<CustomInlet>().OnSamplePulled += updateText;

		feedbackText = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void updateText(float[] sample)
	{
		if (wasPositive && sample[valueChannel] < 0.0f)
		{
			feedbackText.text = "-ve";
			wasPositive = false;
		}
		else if (!wasPositive && sample[valueChannel] >= 0)
		{
			feedbackText.text = "+ve";
			wasPositive = true;
		}
	}
}
