﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using LSL;
using Assets.LSL4Unity.Scripts;
using Assets.LSL4Unity.Scripts.Common;

/* This class is used to control the flow of the Neuro Target Practice Game.
 * Author: Anita Popescu
 * Sachs Lab, 2016.
 */

public class FindTargetTaskController : MonoBehaviour {

    //LSL material.

    private static int ChannelCount = 1;
    private string[] currentSample = new string[ChannelCount];
    public static string StreamName = "FindTargetTaskEvents";
    public static string StreamType = "String";
    public static string UniqueID = "uid98765";

    public liblsl.XMLElement streamInfoXML;
    public liblsl.XMLElement streamInfoXMLFromExternal;

    private liblsl.StreamInfo streamInfo;
    private liblsl.StreamOutlet outlet;
    public liblsl.StreamInfo GetStreamInfo()
    {
        return streamInfo;
    }

    /*public static float STIM1TIMER = 2f;
    public static float STIM2TIMER = 5f;
    public static float FEEDBACKTIMER = 3f;
    public static float ENDGAMETIMER = 5f;
    public static int NUMTRIALS = 5;*/
    public float STIM1TIMER;
    public float STIM2TIMER;
    public float FEEDBACKTIMER;
    public float ENDGAMETIMER;
    public int NUMTRIALS;

    public static string[] configKeys = { "STIM1TIMER", "STIM2TIMER", "FEEDBACKTIMER", "ENDGAMETIMER", "NUMTRIALS" };
    public float[] configValues;

    protected enum Stage {init, stim1, stim2, feedback, reset};
    protected Stage gameState = Stage.init;

    //public GameObject hub;
    public GameObject target;
    public GameObject activeTarget;
    public Text feedbackText;
    public TargetField targetField;
    public float stageTime = 0;
    public int score = 0;
    private bool isHome = false;
    private bool initPresented;
    private bool stim1Presented;
    private bool stim2Presented;
    private bool feedbackPresented;
    private bool roundSuccess;
    private bool start = true;
    private bool endedTrials;
    
    private int currentTrial = 1;

    // Use this for initialization
    private void Awake()
    {
        currentSample = new string[ChannelCount];
        streamInfo = new liblsl.StreamInfo(StreamName, StreamType, ChannelCount, liblsl.IRREGULAR_RATE, liblsl.channel_format_t.cf_string, UniqueID);
        Debug.Log(liblsl.channel_format_t.cf_string);
        streamInfoXML = streamInfo.desc();
    }

    protected void Start () {

        streamInfoXML.append_child_value("SceneLevelName", SceneManager.GetSceneAt(1).name); //returns the next loaded scene after the Control Panel

        if (targetField == null)
        {
            targetField = GameObject.Find("TargetField").GetComponent<TargetField>();
        }

        streamInfoXMLFromExternal = targetField.getHeaderData();
        //streamInfoXMLFromExternal.append_copy();
        streamInfoXML.append_copy(streamInfoXMLFromExternal);

        liblsl.XMLElement eventCodes = streamInfoXML.append_child("EventCodes");
        loadEventCodes(eventCodes);

        // Subscribe to targetField OnAddTarget event.
        // Subscribe to targetField OnRemoveTarget event
        if (feedbackText == null)
        {
            feedbackText = GameObject.FindWithTag("FeedbackText").GetComponent<Text>();
        }

        Debug.Log(streamInfo.as_xml());

        //Read from config file to set timers and # of trials
        loadConfig();
        outlet = new liblsl.StreamOutlet(streamInfo);
        pushSample("011000"+ SceneManager.GetSceneAt(1).name);
        resetStage();
	}
	
    public void pushSample(string sample)
    {
        currentSample[0] = sample;
        outlet.push_sample(currentSample, liblsl.local_clock());
        Debug.Log(sample);
    }

    bool loadEventCodes(liblsl.XMLElement eventCodes)
    {
        string line;
        int counter = 0;
        StreamReader theReader = new StreamReader("Assets/event-codes.txt", Encoding.Default);
        using (theReader)
        {
            do
            {
                line = theReader.ReadLine();
                liblsl.XMLElement eventInstance = eventCodes.append_child("Event");

                if (line != null)
                {
                    string[] entries = line.Split(':');
                    if (entries.Length > 1)
                    {
                        eventInstance.append_child_value("ID", entries[0]);
                        eventInstance.append_child_value("Description", entries[1]);
                    }

                }
                counter++;
            }
            while (line != null);
        }
        return true;
    }

    bool loadConfig ()
    {
        configValues = new float[configKeys.Length];
        string line;
        int counter = 0;
        StreamReader theReader = new StreamReader("Assets/unity-config.txt", Encoding.Default);
        using (theReader)
        {
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    string[] entries = line.Split(',');
                    if (entries.Length > 1)
                        try
                        {
                            configValues[counter] = float.Parse(entries[1]);
                        }
                        catch
                        {
                            configValues[counter] = 3; //set default timer to 3s
                        }
                        
                }
                counter++;
            }
            while (line != null);   
            theReader.Close();
            STIM1TIMER = configValues[0];
            STIM2TIMER = configValues[1];
            FEEDBACKTIMER = configValues[2];
            ENDGAMETIMER = configValues[3];
            NUMTRIALS = (int)configValues[4];
            return true;
        }
    }

	// Update is called once per frame
	protected void Update () {
        if(start)
        {
            resetStage();
            start = false;
        }
        stageTime += Time.deltaTime;

        if (gameState == Stage.init)
        {
            if (!initPresented)
            {
                pushSample("010000");
                chooseActiveTarget();
            }
            if (isHome)
            {
                gameState++;
            }
        }
        else if (gameState == Stage.stim1)
        {
            if (!stim1Presented)
            {
                pushSample("010001");
                stageTime = 0f;
                presentFirstStimulus();
            }
            
            if(stageTime>= STIM1TIMER)
            {
                gameState++;
            }
            else
            {
                
            }
        }
        else if(gameState == Stage.stim2)
        {
            if (!stim2Presented)
            {
                pushSample("010002");
                stageTime = 0f;
                presentSecondStimulus();
            }

            if(stageTime >= STIM2TIMER||roundSuccess)
            {
                gameState++;
            }
        }
        else if(gameState == Stage.feedback)
        {
            if(!feedbackPresented)
            {
                pushSample("010003");
                presentFeedback();
                stageTime = 0;
            }
            if (stageTime >= FEEDBACKTIMER)
            {
                gameState++;
            }
        }
        else if(gameState == Stage.reset)
        {
            if(!(currentTrial == NUMTRIALS))
            {
                gameState = Stage.init;
                resetStage();
                pushSample("010005" + currentTrial);
                currentTrial++;
            }
            else
            {
                if(!endedTrials)
                {
                    endGame();
                    stageTime = 0;
                }
                if (stageTime >= ENDGAMETIMER)
                {
                    //TODO: if simple, load complex; if complex, load simple
                    outlet = null;
                    if(SceneManager.GetSceneAt(1).name == "TaskFindTarget")
                    {
                        SceneManager.UnloadSceneAsync("TaskFindTarget");
                        SceneManager.LoadScene("TaskFindTargetComplex", LoadSceneMode.Additive);
                    }
                    else if(SceneManager.GetSceneAt(1).name == "TaskFindTargetComplex")
                    {
                        SceneManager.UnloadSceneAsync("TaskFindTargetComplex");
                        SceneManager.LoadScene("TaskFindTarget", LoadSceneMode.Additive);
                    }
                }
            }
        }
    }

    //Called by the target when it is triggered
    public void TargetSelected()
    {
        pushSample("010006");
        if (gameState == Stage.stim2)
        {
            roundSuccess = true;
        }
    }

    //Called by HomeBase when mouse moves off or onto the center
    public void setIsHome (bool trigger)
    {
        if(trigger)
        {
            pushSample("010007");
        } else
        {
            pushSample("010008");
        }
        isHome = trigger;
        if(!trigger && gameState==Stage.stim1)
        {
            DepartedCenterEarly();
            pushSample("010009");
        }
    }

    //If the mouse was moved off the homebase and the state was stim1,
    //then the user moved too soon. The result in the described method
    //is to restart the current trial, but another implementation may
    //also set the 
    void DepartedCenterEarly()
    {
        resetStage();
        gameState = Stage.init;
        feedbackText.text = "You moved too soon! Restarting trial.";
    }

    //Creates a target or selects from a set of targets on the screen
    protected void chooseActiveTarget()
    {
        target = targetField.getRandomTarget();
        initPresented = true;
    }

    //Presents the planning stimulus
    void presentFirstStimulus()
    {
        // presents the first stimulus e.g shake/rotate
        target.GetComponent<Renderer>().material.color = Color.green;
        pushSample("010010");
        stim1Presented = true;
        feedbackText.text = "";
    }

    //Presents the execution stimulus
    void presentSecondStimulus()
    {
        // presents the second stimulus e.g auditory cue or colour change
        pushSample("010011");
        target.GetComponent<AudioSource>().Play();
        stim2Presented = true;
        feedbackText.text = "";
    }

    //Presents textual feedback to user about the result of the trial
    void presentFeedback()
    {
        if (roundSuccess)
        {
            feedbackText.text = "Nice job!";
            score++;
        }
        else
        {
            feedbackText.text = "Oops, too slow!";
        }
        pushSample("010014" + roundSuccess);
        feedbackPresented = true;
    }

    void resetStage()
    {
        initPresented = false;
        stim1Presented = false;
        stim2Presented = false;
        feedbackPresented = false;
        roundSuccess = false;
        if(target)
        {
            target.GetComponent<Renderer>().material.color = Color.white;
        }
        
        targetField.clearTarget();
        feedbackText.text = "Please return to the center.";
    }

    //Called when the number of trials is reached
    void endGame()
    {
        //Present Game Over animation
        feedbackText.text = "Game Over! You succeeded in "+score+" out of "+NUMTRIALS+" trials.";
        endedTrials = true;
        pushSample("010012");
        pushSample("010013(" + score + "," + NUMTRIALS + ")");
    }

    protected Stage getGameState()
    {
        return gameState;
    }


}
