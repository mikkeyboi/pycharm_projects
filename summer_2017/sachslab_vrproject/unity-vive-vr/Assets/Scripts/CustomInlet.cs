﻿using UnityEngine;
using Assets.LSL4Unity.Scripts.AbstractInlets;
using Assets.LSL4Unity.Scripts;

public class CustomInlet : InletFloatSamples
{

	public delegate void SamplePulled(float[] lastSample);
	public event SamplePulled OnSamplePulled;

	private bool hasNewSample = false;
	private float[] lastSample;

    // Update is called once per frame
    void Update () {
        if (inlet != null)
        {
            pullSamples();

			if (hasNewSample) {
				// Debug.Log(lastSample[0]);

				if (OnSamplePulled != null) {
					OnSamplePulled (lastSample);
				}
				hasNewSample = false;
			}
        }
    }

    protected override void OnStreamAvailable()
    {
		Debug.Log(string.Format("CustomInlet OnStreamAvailable() triggered with stream {0}", inlet.info().name()));
    }

    protected override void Process(float[] newSample, double timeStamp)
    {
		lastSample = newSample;
		hasNewSample = true;
    }
}
