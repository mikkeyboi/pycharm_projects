﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTaskControllerComplex : FindTargetTaskController {
    
    public GameObject Prefab1;
    public GiftPosition giftPos;
    public Renderer hand;
    private static int NUMGIFTS = 3;
    GameObject[] gifts = new GameObject[NUMGIFTS];
    private bool created;
	// Use this for initialization

	new void Start () {
        base.Start();
        /*if(giftPos == null)
        {
            giftPos = GameObject.FindGameObjectWithTag("Gift").GetComponent<GiftPosition>();
        }*/
        hand = GameObject.FindGameObjectWithTag("LeftHand").GetComponent<Renderer>();
        for (int p = 0; p < gifts.Length; p++)
        {
            gifts[p] = Resources.Load("Gifts/Gift" + (p+1)) as GameObject;
        }
        created = false;
        //createGift();
    }

    // Update is called once per frame
    new void Update () {
        base.Update();
        if (gameState == Stage.stim2)
        {
            if (!created)
            {
                createGift();
                giftPos.setControl(true);
                created = true;
                hand.enabled = false;
            }
        }
        if (gameState == Stage.stim2)
        {
            if(!created)
            {
                createGift();
                giftPos.setControl(true);
                created = true;
            }
            
        }
        if (gameState == Stage.feedback)
        {
            giftPos.setControl(false);
            created = false;
            hand.enabled = true;
        }
        /*else
        {
            giftPos.setControl(true);
        }*/
	}

    new void chooseActiveTarget()
    {
        base.chooseActiveTarget();
        int doorID = targetField.getActiveTargetID();
        GameObject activeDoor = GameObject.Find("Door" + doorID);
    }

    void createGift()
    {
        int prefabIndex = UnityEngine.Random.Range(0,gifts.Length);
        pushSample("050001(" + (prefabIndex + 1) + ")");
        GameObject gift = (GameObject)Instantiate(gifts[prefabIndex]);
        giftPos = gift.GetComponent<GiftPosition>();
    }
}
