# unity-vive-vr
This repository hosts the Unity project frameworks for performing VR tasks, integrated with LSL.

# How it Works
When selecting to run the UROP trials, one of two VR environments will appear.

## First task (simple) 
“BlueSky” task, shows three equidistant, white target spheres aligned horizontally in front of the subject. 
- **Preparation cue:** after the subject places their VR controller, rendered as a grey sphere, over the central target, one of the left or right targets undergoes a discrete colour change from white to green, becoming activated. 
- **Imperative cue:** three seconds later, an auditory stimulus is presented and the subject moves their controller to cover the activated target within a five-second period to succeed in the trial. 

## Second task (Enriched): 
“Workshop” task, renders the same targets and controller along with a room that imitates a “Santa’s Workshop” environment and plays thematic music. 
- **Preparation cue:** identical to BlueSky task.
- **Imperative cue:** identical to BlueSky task except that auditory stimulus is paired with a discrete transformation of the grey sphere into one of three meshes shaped like a teddy bear, a candy cane, or a star, but contain the same collider shape as the grey sphere.

Demo: https://www.youtube.com/watch?v=LDQ9w3Af20o

# Notes
- If you are using SteamVR, start up the SteamVR client before opening the project in Unity.
